
#ifndef RATIONNALVARIABLE_H
#define RATIONNALVARIABLE_H

#include "VariableAbstract.h"

#include <string>


/**
  * class RationnalVariable
  * 
  */

class RationnalVariable : public VariableAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  RationnalVariable();

  /**
   * Empty Destructor
   */
  virtual ~RationnalVariable();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Display the number as a string.
   * @return std::string
   */
  std::string print() const
  {
  }


  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(std::string target) const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  IntegerVariable numerator;
  IntegerVariable denominator;

  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  /**
   * Set the value of numerator
   * @param value the new value of numerator
   */
  void setNumerator(IntegerVariable value)
  {
    numerator = value;
  }

  /**
   * Get the value of numerator
   * @return the value of numerator
   */
  IntegerVariable getNumerator()
  {
    return numerator;
  }

  /**
   * Set the value of denominator
   * @param value the new value of denominator
   */
  void setDenominator(IntegerVariable value)
  {
    denominator = value;
  }

  /**
   * Get the value of denominator
   * @return the value of denominator
   */
  IntegerVariable getDenominator()
  {
    return denominator;
  }

  void initAttributes();

};

#endif // RATIONNALVARIABLE_H
