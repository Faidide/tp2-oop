
#ifndef EXPFUNCTION_H
#define EXPFUNCTION_H

#include "MathFunctionAbstract.h"

#include <string>


/**
  * class ExpFunction
  * 
  */

class ExpFunction : public MathFunctionAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  ExpFunction();

  /**
   * Empty Destructor
   */
  virtual ~ExpFunction();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Return the variable (self).
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify()
  {
  }


  /**
   * Return the string representing the variable.
   * @return std::string
   */
  std::string print()
  {
  }


  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(std::string target)
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // EXPFUNCTION_H
