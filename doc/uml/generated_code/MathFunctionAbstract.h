
#ifndef MATHFUNCTIONABSTRACT_H
#define MATHFUNCTIONABSTRACT_H

#include "ArythmeticAbstract.h"
#include "FunctionAbstract.h"

#include <string>


/**
  * class MathFunctionAbstract
  * 
  */

class MathFunctionAbstract : virtual public ArythmeticAbstract, public FunctionAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  MathFunctionAbstract();

  /**
   * Empty Destructor
   */
  virtual ~MathFunctionAbstract();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Simplifies what's between the parenthesis and if possible the self.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const
  {
  }


  /**
   * Print the expression.
   * @return std::string
   */
  std::string print()
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  

  // Represents the expression between the two parenthesis calling the function. This is resolved when simplifying. For example this expression might bo a MultiplyOperator with IntegerVariable representing 4 and 5 inside if this object is a sin(4*5) expression.
  std::shared_ptr<ArythmeticAbstract> exprInside;

  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of exprInside
   * Represents the expression between the two parenthesis calling the function. This
   * is resolved when simplifying. For example this expression might bo a
   * MultiplyOperator with IntegerVariable representing 4 and 5 inside if this object
   * is a sin(4*5) expression.
   * @param value the new value of exprInside
   */
  void setExprInside(std::shared_ptr<ArythmeticAbstract> value)
  {
    exprInside = value;
  }

  /**
   * Get the value of exprInside
   * Represents the expression between the two parenthesis calling the function. This
   * is resolved when simplifying. For example this expression might bo a
   * MultiplyOperator with IntegerVariable representing 4 and 5 inside if this object
   * is a sin(4*5) expression.
   * @return the value of exprInside
   */
  std::shared_ptr<ArythmeticAbstract> getExprInside()
  {
    return exprInside;
  }

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  void initAttributes();

};

#endif // MATHFUNCTIONABSTRACT_H
