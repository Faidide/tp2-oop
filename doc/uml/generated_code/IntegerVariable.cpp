#include "IntegerVariable.h"

// Constructors/Destructors
//  

IntegerVariable::IntegerVariable()
{
  initAttributes();
}

IntegerVariable::~IntegerVariable()
{
}

//  
// Methods
//  


// Accessor methods
//  


// Other methods
//  

void IntegerVariable::initAttributes()
{
  value = 0;
}

