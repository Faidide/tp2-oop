
#ifndef ARYTHMETICABSTRACT_H
#define ARYTHMETICABSTRACT_H

#include <string>


/**
  * class ArythmeticAbstract
  * Abstract base class for all arythmetics expression members.
  */

class ArythmeticAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  ArythmeticAbstract();

  /**
   * Empty Destructor
   */
  virtual ~ArythmeticAbstract();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Allow to simplify, evaluate and return the arythmetic expression at maximum.
   * @return ArythmeticAbstract
   */
  virtual std::shared_ptr<ArythmeticAbstract> simplify() const = 0;


  /**
   * Return the string representation the arythmetic operation.
   * @return std::string
   */
  virtual std::string print() const = 0;


  /**
   * Derivates the expression using a variable name.
   * @return ArythmeticAbstract
   * @param  target the parameter to derivate from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(std::string target = x) const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  

  void Reduced;

  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of Reduced
   * @param value the new value of Reduced
   */
  void setReduced(void value)
  {
    Reduced = value;
  }

  /**
   * Get the value of Reduced
   * @return the value of Reduced
   */
  void getReduced()
  {
    return Reduced;
  }

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  void initAttributes();

};

#endif // ARYTHMETICABSTRACT_H
