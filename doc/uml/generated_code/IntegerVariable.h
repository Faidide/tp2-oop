
#ifndef INTEGERVARIABLE_H
#define INTEGERVARIABLE_H

#include "VariableAbstract.h"

#include <string>


/**
  * class IntegerVariable
  * 
  */

class IntegerVariable : public VariableAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  IntegerVariable();

  /**
   * Empty Destructor
   */
  virtual ~IntegerVariable();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Override displaying function
   * @return std::string
   */
  std::string print() const
  {
  }


  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(std::string target) const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  int value;

  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  /**
   * Set the value of value
   * @param value the new value of value
   */
  void setValue(int value)
  {
    value = value;
  }

  /**
   * Get the value of value
   * @return the value of value
   */
  int getValue()
  {
    return value;
  }

  void initAttributes();

};

#endif // INTEGERVARIABLE_H
