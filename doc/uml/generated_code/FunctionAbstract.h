
#ifndef FUNCTIONABSTRACT_H
#define FUNCTIONABSTRACT_H

#include "ArythmeticAbstract.h"

#include <string>


/**
  * class FunctionAbstract
  * 
  */

class FunctionAbstract : virtual public ArythmeticAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  FunctionAbstract();

  /**
   * Empty Destructor
   */
  virtual ~FunctionAbstract();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // FUNCTIONABSTRACT_H
