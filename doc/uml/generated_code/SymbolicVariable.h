
#ifndef SYMBOLICVARIABLE_H
#define SYMBOLICVARIABLE_H

#include "VariableAbstract.h"

#include <string>


/**
  * class SymbolicVariable
  * 
  */

class SymbolicVariable : public VariableAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  SymbolicVariable();

  /**
   * Empty Destructor
   */
  virtual ~SymbolicVariable();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Return the variable (self).
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const
  {
  }


  /**
   * Return the string representing the variable.
   * @return std::string
   */
  std::string print() const
  {
  }


  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(std::string target)
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // SYMBOLICVARIABLE_H
