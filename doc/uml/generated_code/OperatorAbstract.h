
#ifndef OPERATORABSTRACT_H
#define OPERATORABSTRACT_H

#include "ArythmeticAbstract.h"

#include <string>


/**
  * class OperatorAbstract
  * 
  */

/******************************* Abstract Class ****************************
OperatorAbstract does not have any pure virtual methods, but its author
  defined it as an abstract class, so you should not use it directly.
  Inherit from it instead and create only objects from the derived classes
*****************************************************************************/

class OperatorAbstract : virtual public ArythmeticAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  OperatorAbstract();

  /**
   * Empty Destructor
   */
  virtual ~OperatorAbstract();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Override the simplification function of all the arythmetic operation. eg: return
   * the result as simplified as we possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const
  {
  }


  /**
   * Override the function to print the operation.
   * @return std::string
   */
  std::string print() const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  

  // Left part of the operator expression.
  std::shared_ptr<ArythmeticAbstract> LeftOperand;
  // Right part of the operands.
  std::shared_ptr<ArythmeticAbstract> RightOperand;
  std::string stringRep;

  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //  


  /**
   * Set the value of LeftOperand
   * Left part of the operator expression.
   * @param value the new value of LeftOperand
   */
  void setLeftOperand(std::shared_ptr<ArythmeticAbstract> value)
  {
    LeftOperand = value;
  }

  /**
   * Get the value of LeftOperand
   * Left part of the operator expression.
   * @return the value of LeftOperand
   */
  std::shared_ptr<ArythmeticAbstract> getLeftOperand()
  {
    return LeftOperand;
  }

  /**
   * Set the value of RightOperand
   * Right part of the operands.
   * @param value the new value of RightOperand
   */
  void setRightOperand(std::shared_ptr<ArythmeticAbstract> value)
  {
    RightOperand = value;
  }

  /**
   * Get the value of RightOperand
   * Right part of the operands.
   * @return the value of RightOperand
   */
  std::shared_ptr<ArythmeticAbstract> getRightOperand()
  {
    return RightOperand;
  }

  /**
   * Set the value of stringRep
   * @param value the new value of stringRep
   */
  void setStringRep(std::string value)
  {
    stringRep = value;
  }

  /**
   * Get the value of stringRep
   * @return the value of stringRep
   */
  std::string getStringRep()
  {
    return stringRep;
  }

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  void initAttributes();

};

#endif // OPERATORABSTRACT_H
