
#ifndef VARIABLEABSTRACT_H
#define VARIABLEABSTRACT_H

#include "ArythmeticAbstract.h"

#include <string>


/**
  * class VariableAbstract
  * 
  */

class VariableAbstract : virtual public ArythmeticAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  VariableAbstract();

  /**
   * Empty Destructor
   */
  virtual ~VariableAbstract();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Override the simplification function used to return the expression and compute
   * it as much as we can.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const
  {
  }


  /**
   * Display function, virtual.
   */
  void print() const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // VARIABLEABSTRACT_H
