
#ifndef ADDOPERATOR_H
#define ADDOPERATOR_H

#include "OperatorAbstract.h"

#include <string>


/**
  * class AddOperator
  * 
  */

class AddOperator : virtual public OperatorAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  AddOperator();

  /**
   * Empty Destructor
   */
  virtual ~AddOperator();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Simplify the operator left and right operand and try to add the two if possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify()
  {
  }


  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(std::string target) const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // ADDOPERATOR_H
