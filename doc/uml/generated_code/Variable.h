
#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>


/**
  * class Variable
  * 
  */

class Variable
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Variable();

  /**
   * Empty Destructor
   */
  virtual ~Variable();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Return the variable (self).
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const
  {
  }


  /**
   * Return the string representing the variable.
   * @return std::string
   */
  std::string print() const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // VARIABLE_H
