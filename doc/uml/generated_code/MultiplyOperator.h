
#ifndef MULTIPLYOPERATOR_H
#define MULTIPLYOPERATOR_H

#include "OperatorAbstract.h"

#include <string>


/**
  * class MultiplyOperator
  * 
  */

class MultiplyOperator : virtual public OperatorAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  MultiplyOperator();

  /**
   * Empty Destructor
   */
  virtual ~MultiplyOperator();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Simplify the operator left and right operand and try to add the two if possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify()
  {
  }


  /**
   * Compute the derivate given a variable name.
   * @return ArythmeticAbstract
   * @param  target Variable to derivate the expression with.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(std::string target) const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // MULTIPLYOPERATOR_H
