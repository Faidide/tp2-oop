
#ifndef DIVIDEOPERATOR_H
#define DIVIDEOPERATOR_H

#include "OperatorAbstract.h"

#include <string>


/**
  * class DivideOperator
  * 
  */

class DivideOperator : virtual public OperatorAbstract
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  DivideOperator();

  /**
   * Empty Destructor
   */
  virtual ~DivideOperator();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   * Simplify the operator left and right operand and try to add the two if possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const
  {
  }


  /**
   * Derivates from the target variable.
   * @return ArythmeticAbstract
   * @param  target Variable name to derivate from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(std::string target) const
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // DIVIDEOPERATOR_H
