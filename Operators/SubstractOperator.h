
#ifndef SUBSTRACTOPERATOR_H
#define SUBSTRACTOPERATOR_H

#include <string>

#include "Operators/OperatorAbstract.h"

/**
 * class SubstractOperator
 *
 */

class SubstractOperator : virtual public OperatorAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  SubstractOperator(std::shared_ptr<ArythmeticAbstract> leftOperandI,
                    std::shared_ptr<ArythmeticAbstract> rightOperandI);
  SubstractOperator(int a, int b) : OperatorAbstract(a, b) { stringRep = "-"; };

  /**
   * Empty Destructor
   */
  virtual ~SubstractOperator();

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  /**
   * Simplify the operator left and right operand and try to add the two if
   * possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const override final;

  /**
   * Compute the derivative as a new expresison given target.
   *
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(
    std::string target) const override final;

  /**
   * Replace some expression x with another y inside the ArythmeticAbstract
   *
   */
  std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract> x,
    std::shared_ptr<ArythmeticAbstract> y);

  /**
   * Evaluate the float value of the expression
   * @return the floating point value of the expression
   */
  float evaluate() const;

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //
};

#endif  // SUBSTRACTOPERATOR_H
