#include "MultiplyOperator.h"

#include "Operators/AddOperator.h"
#include "Operators/DivideOperator.h"

// Constructors/Destructors
//

MultiplyOperator::MultiplyOperator(
  std::shared_ptr<ArythmeticAbstract> leftOperandI,
  std::shared_ptr<ArythmeticAbstract> rightOperandI)
  : OperatorAbstract(leftOperandI, rightOperandI) {
  stringRep = "*";
}

MultiplyOperator::~MultiplyOperator() {}

//
// Methods
/**
 * Return the variable (self).
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> MultiplyOperator::simplify() const {
  // get the simplification for left and right side of operand
  std::shared_ptr<ArythmeticAbstract> lSimplified = LeftOperand->simplify();
  std::shared_ptr<ArythmeticAbstract> rSimplified = RightOperand->simplify();

  // try to cast the simplified values to integer expressions
  IntegerVariable *lInt = dynamic_cast<IntegerVariable *>(lSimplified.get());
  IntegerVariable *rInt = dynamic_cast<IntegerVariable *>(rSimplified.get());
  // if they are both integers
  if (lInt != nullptr && rInt != nullptr) {
    // return a new integer value with result inside
    return std::make_shared<IntegerVariable>(lInt->getValue() *
                                             rInt->getValue());
  }
  // if one integer is equal to one, return the other ArythmeticAbstract
  if (lInt != nullptr && lInt->getValue() == 1) {
    return rSimplified;
  }
  if (rInt != nullptr && rInt->getValue() == 1) {
    return lSimplified;
  }
  // if one integer is equal to zero, return zero
  if (lInt != nullptr && lInt->getValue() == 0) {
    return std::make_shared<IntegerVariable>(0);
  }
  if (rInt != nullptr && rInt->getValue() == 0) {
    return std::make_shared<IntegerVariable>(0);
  }

  // try to cast em to divisionOperators
  DivideOperator *lDiv = dynamic_cast<DivideOperator *>(lSimplified.get());
  DivideOperator *rDiv = dynamic_cast<DivideOperator *>(rSimplified.get());
  // if both are divisionOperator
  if (lDiv != nullptr && rDiv != nullptr) {
    // multiply with the * operator for divs, return the simplified version
    return ((*lDiv) * (*rDiv))->simplify();
  }
  // if left one is integer and right one div
  if (lInt != nullptr && rDiv != nullptr) {
    // return the result from the * operator
    return ((*rDiv) * (*lInt))->simplify();
  }
  // if right one is integer and left one a div
  if (lDiv != nullptr && rInt != nullptr) {
    // return the result from the * operator
    return ((*lDiv) * (*rInt))->simplify();
  }

  // implementing distrubutivity to add in case we have an integer around here:
  // (a+b)*c = a*c + b*c
  // it might mess up with other simplification so we do it only if we got
  // integers we can later simplify
  IntegerVariable *pInt = nullptr;
  AddOperator *pAdd = nullptr;
  // if left is integer (the other will not be due to previous behaviour)
  if (lInt != nullptr) {
    // if right can be casted to an add operator and contains an integer
    pInt = dynamic_cast<IntegerVariable *>(rSimplified->findSummedInteger());
    pAdd = dynamic_cast<AddOperator *>(rSimplified.get());
    if (pInt != nullptr && pAdd != nullptr) {
      // then we need to replace both inner terms with the result of product to
      // the int create a mult with lInt*rSimp->Right
      std::shared_ptr<MultiplyOperator> rMul =
        std::make_shared<MultiplyOperator>(lSimplified,
                                           pAdd->getRightOperand());
      // create a mult with lInt*rSimp->Left
      std::shared_ptr<MultiplyOperator> lMul =
        std::make_shared<MultiplyOperator>(lSimplified, pAdd->getLeftOperand());
      // create a sum with the two mult and return simplified result
      return std::make_shared<AddOperator>(lMul, rMul)->simplify();
    }
  }
  // same the other way around
  pInt = nullptr;
  pAdd = nullptr;
  // if left is integer (the other will not be due to previous behaviour)
  if (rInt != nullptr) {
    // if right can be casted to an add operator and contains an integer
    pInt = dynamic_cast<IntegerVariable *>(lSimplified->findSummedInteger());
    pAdd = dynamic_cast<AddOperator *>(lSimplified.get());
    if (pInt != nullptr && pAdd != nullptr) {
      // then we need to replace both inner terms with the result of product to
      // the int create a mult with lInt*rSimp->Right
      std::shared_ptr<MultiplyOperator> rMul =
        std::make_shared<MultiplyOperator>(rSimplified,
                                           pAdd->getRightOperand());
      // create a mult with lInt*rSimp->Left
      std::shared_ptr<MultiplyOperator> lMul =
        std::make_shared<MultiplyOperator>(rSimplified, pAdd->getLeftOperand());
      // create a sum with the two mult and return simplified result
      return std::make_shared<AddOperator>(lMul, rMul)->simplify();
    }
  }

  // now we try to group integer terms together in a multiplication
  // try to cast to multiplyOperators
  MultiplyOperator *lMul = dynamic_cast<MultiplyOperator *>(lSimplified.get());
  MultiplyOperator *rMul = dynamic_cast<MultiplyOperator *>(rSimplified.get());
  // if left is an integer and right a multiplication
  if (lInt != nullptr && rMul != nullptr) {
    // get the first integer that multiplies right operand
    IntegerVariable *mulP =
      dynamic_cast<IntegerVariable *>(rSimplified->findIntegerMultiple());
    // if it's not null
    if (mulP != nullptr) {
      // multiply lInt and the integer to replace it and have less one member
      mulP->setValue(lInt->getValue() * mulP->getValue());
      // return the result
      return rSimplified->simplify();
    }
  }
  if (rInt != nullptr && lMul != nullptr) {
    // get the first integer that multiplies right operand
    IntegerVariable *mulP =
      dynamic_cast<IntegerVariable *>(lSimplified->findIntegerMultiple());
    // if it's not null
    if (mulP != nullptr) {
      // multiply lInt and the integer to replace it and have less one member
      mulP->setValue(rInt->getValue() * mulP->getValue());
      // return the result
      return lSimplified->simplify();
    }
  }

  // if we reach here it was not possible to make it into something
  // then, return simplified multiplication
  return std::make_shared<MultiplyOperator>(lSimplified, rSimplified);
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> MultiplyOperator::derivative(
  std::string target) const {
  // return derivative of left side plus the rigt side one
  std::shared_ptr<ArythmeticAbstract> leftDeriv =
    LeftOperand->derivative(target)->simplify();
  std::shared_ptr<ArythmeticAbstract> rightDeriv =
    RightOperand->derivative(target)->simplify();

  // (U'V+UV')
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(leftDeriv, RightOperand);
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(rightDeriv, LeftOperand);

  // build and return the result
  return std::make_shared<AddOperator>(mul1, mul2)->simplify();
}

//

// return self for integers
ArythmeticAbstract *MultiplyOperator::findIntegerMultiple() {
  IntegerVariable *intV = nullptr;

  // if left is integer return a pointer to it
  intV = dynamic_cast<IntegerVariable *>(LeftOperand.get());
  if (intV != nullptr) {
    return intV;
  }

  // if right is integer return a pointer to it
  intV = dynamic_cast<IntegerVariable *>(RightOperand.get());
  if (intV != nullptr) {
    return intV;
  }

  // call recursively on left
  intV = dynamic_cast<IntegerVariable *>(LeftOperand->findIntegerMultiple());
  // if it found something return it
  if (intV != nullptr) {
    return intV;
  }

  // call recursively on right
  intV = dynamic_cast<IntegerVariable *>(RightOperand->findIntegerMultiple());
  // if it found something return it
  if (intV != nullptr) {
    return intV;
  }

  // if nothing was found return nullptr
  return nullptr;
}

std::shared_ptr<ArythmeticAbstract> MultiplyOperator::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else, return a new substraction with both side substituted
  return std::make_shared<MultiplyOperator>(LeftOperand->substitute(x, y),
                                            RightOperand->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float MultiplyOperator::evaluate() const {
  return (LeftOperand->evaluate() * RightOperand->evaluate());
}