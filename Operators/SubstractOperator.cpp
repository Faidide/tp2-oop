#include "SubstractOperator.h"

#include "Operators/AddOperator.h"
#include "Operators/MultiplyOperator.h"

// Constructors/Destructors
//

SubstractOperator::SubstractOperator(
  std::shared_ptr<ArythmeticAbstract> leftOperandI,
  std::shared_ptr<ArythmeticAbstract> rightOperandI)
  : OperatorAbstract(leftOperandI, rightOperandI) {
  stringRep = "-";
}

SubstractOperator::~SubstractOperator() {}

//
// Methods
/**
 * Return a simplified version.
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> SubstractOperator::simplify() const {
  // we will rely on other's operators simplification to avoid duplicating
  // implementations: (a-b)=(a+((-1)*b))
  return std::make_shared<AddOperator>(
           LeftOperand, std::make_shared<MultiplyOperator>(
                          std::make_shared<IntegerVariable>(-1), RightOperand))
    ->simplify();
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> SubstractOperator::derivative(
  std::string target) const {
  // return derivative of left side plus the rigt side one
  std::shared_ptr<ArythmeticAbstract> leftDeriv =
    LeftOperand->derivative(target)->simplify();
  std::shared_ptr<ArythmeticAbstract> rightDeriv =
    RightOperand->derivative(target)->simplify();
  // return the sum
  return std::make_shared<SubstractOperator>(leftDeriv, rightDeriv)->simplify();
}
//

std::shared_ptr<ArythmeticAbstract> SubstractOperator::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else, return a new substraction with both side substituted
  return std::make_shared<SubstractOperator>(LeftOperand->substitute(x, y),
                                             RightOperand->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float SubstractOperator::evaluate() const {
  return (LeftOperand->evaluate() - RightOperand->evaluate());
}