
#ifndef DIVIDEOPERATOR_H
#define DIVIDEOPERATOR_H

#include <string>

#include "Operators/OperatorAbstract.h"

/**
 * class DivideOperator
 *
 */

class DivideOperator : virtual public OperatorAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  DivideOperator(std::shared_ptr<ArythmeticAbstract> leftOperandI,
                 std::shared_ptr<ArythmeticAbstract> rightOperandI);

  DivideOperator(int a, int b) : OperatorAbstract(a, b) { stringRep = "/"; };

  /**
   * Empty Destructor
   */
  virtual ~DivideOperator();

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  /**
   * Simplify the operator left and right operand and try to add the two if
   * possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const override final;

  /**
   * Derivates from the target variable.
   * @return ArythmeticAbstract
   * @param  target Variable name to derivate from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(
    std::string target) const override final;

  /**
   * Replace some expression x with another y inside the ArythmeticAbstract
   *
   */
  std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract> x,
    std::shared_ptr<ArythmeticAbstract> y);

  /**
   * operators
   */
  std::shared_ptr<ArythmeticAbstract> operator+(IntegerVariable& b);
  std::shared_ptr<ArythmeticAbstract> operator+(DivideOperator& b);
  std::shared_ptr<ArythmeticAbstract> operator+(ArythmeticAbstract& b);
  std::shared_ptr<ArythmeticAbstract> operator*(IntegerVariable& b);
  std::shared_ptr<ArythmeticAbstract> operator*(DivideOperator& b);
  std::shared_ptr<ArythmeticAbstract> operator*(ArythmeticAbstract& b);

  /**
   * Evaluate the float value of the expression
   * @return the floating point value of the expression
   */
  float evaluate() const;

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //
};

#endif  // DIVIDEOPERATOR_H
