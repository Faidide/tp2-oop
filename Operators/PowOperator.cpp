#include "PowOperator.h"

#include <cmath>

#include "Operators/MultiplyOperator.h"
#include "Operators/SubstractOperator.h"

// Constructors/Destructors
//

PowOperator::PowOperator(std::shared_ptr<ArythmeticAbstract> leftOperandI,
                         std::shared_ptr<ArythmeticAbstract> rightOperandI)
  : OperatorAbstract(leftOperandI, rightOperandI) {
  stringRep = "^";
}

PowOperator::~PowOperator() {}

//
// Methods
//
// Methods
/**
 * Return the variable (self).
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> PowOperator::simplify() const {
  // get the simplification for left and right side of operand
  std::shared_ptr<ArythmeticAbstract> lSimplified = LeftOperand->simplify();
  std::shared_ptr<ArythmeticAbstract> rSimplified = RightOperand->simplify();

  // try to cast the simplified values to integer expressions
  IntegerVariable *lInt = dynamic_cast<IntegerVariable *>(lSimplified.get());
  IntegerVariable *rInt = dynamic_cast<IntegerVariable *>(rSimplified.get());

  // if its pow zero, return one
  if (rInt != nullptr && rInt->getValue() == 0) {
    return std::make_shared<IntegerVariable>(1);
  }

  // if its something pow zero (and not 0 pow 0) return 0
  if (lInt != nullptr && lInt->getValue() == 0) {
    return std::make_shared<IntegerVariable>(0);
  }

  // if they are both integers and the power is positive
  if (lInt != nullptr && rInt != nullptr && rInt->getValue() > 0) {
    // return the pow of the two
    return std::make_shared<IntegerVariable>(
      PowOperator::intPow(lInt->getValue(), rInt->getValue()));
  }

  // if nothing worked, return the operator with simplified operands
  return std::make_shared<PowOperator>(lSimplified, rSimplified);
}

int PowOperator::intPow(int x, int p) {
  if (p == 0) return 1;
  if (p == 1) return x;

  int tmp = intPow(x, p / 2);
  if (p % 2 == 0)
    return tmp * tmp;
  else
    return x * tmp * tmp;
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> PowOperator::derivative(
  std::string target) const {
  // return derivative of left side plus the rigt side one
  std::shared_ptr<ArythmeticAbstract> leftDeriv =
    LeftOperand->derivative(target)->simplify();

  // B*A^{B-1}*A'
  // B*A'
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(leftDeriv, RightOperand);
  // A^{B-1}
  std::shared_ptr<SubstractOperator> bm1 = std::make_shared<SubstractOperator>(
    RightOperand, std::make_shared<IntegerVariable>(1));
  std::shared_ptr<PowOperator> pow1 =
    std::make_shared<PowOperator>(LeftOperand, bm1);

  // build and return the result
  return std::make_shared<MultiplyOperator>(mul1, pow1)->simplify();
}

std::shared_ptr<ArythmeticAbstract> PowOperator::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else, return a new substraction with both side substituted
  return std::make_shared<PowOperator>(LeftOperand->substitute(x, y),
                                       RightOperand->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float PowOperator::evaluate() const {
  return std::pow(LeftOperand->evaluate(), RightOperand->evaluate());
}