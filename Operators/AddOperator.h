
#ifndef ADDOPERATOR_H
#define ADDOPERATOR_H

#include <string>

#include "Operators/OperatorAbstract.h"

/**
 * class AddOperator
 *
 */

class AddOperator : virtual public OperatorAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  AddOperator(std::shared_ptr<ArythmeticAbstract> leftOperandI,
              std::shared_ptr<ArythmeticAbstract> rightOperandI);

  AddOperator(int a, int b) : OperatorAbstract(a, b) { stringRep = "+"; };

  /**
   * Empty Destructor
   */
  virtual ~AddOperator();

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  /**
   * Simplify the operator left and right operand and try to add the two if
   * possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const override final;

  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(
    std::string target) const override final;

  /**
   * Return if existing a pointer to an integer directly added to the expression
   * (will be overwritten only in the add operator)
   */
  ArythmeticAbstract* findSummedInteger();

  /**
   * Replace some expression x with another y inside the ArythmeticAbstract
   *
   */
  std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract> x,
    std::shared_ptr<ArythmeticAbstract> y);

  /**
   * Evaluate the float value of the expression
   * @return the floating point value of the expression
   */
  float evaluate() const;

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //
};

#endif  // ADDOPERATOR_H
