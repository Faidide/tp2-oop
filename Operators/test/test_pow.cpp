#include <gmock/gmock.h>

#include <iostream>

#include "Operators/PowOperator.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(MultiplyOperator, creation) {
  // test if sucessfully create a function
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  PowOperator myPow(int1, int2);
}

// testing the object creation
TEST(MultiplyOperator, creation2) {
  // test if sucessfully create a function
  PowOperator myPow(1, 3);
}

// testing the object creation
TEST(MultiplyOperator, simplification) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(2);
  PowOperator myPow(int1, int2);
  try {
    ASSERT_THAT(myPow.simplify()->print() == IntegerVariable(4).print(),
                Eq(true));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// testing the object creation
TEST(MultiplyOperator, print) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  PowOperator myPow(int1, int2);
  ASSERT_THAT(myPow.print(), Eq("(1^1)"));
}

}  // namespace