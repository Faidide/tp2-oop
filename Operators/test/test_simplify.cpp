#include <gmock/gmock.h>

#include <iostream>

#include "Operators/AddOperator.h"
#include "Operators/DivideOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Operators/SubstractOperator.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// (1)+(1/1) should return 2
TEST(Simplify, simplify1) {
  // create division
  std::shared_ptr<DivideOperator> div = std::make_shared<DivideOperator>(1, 1);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create final sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("2"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+(2/1) should return 3
TEST(Simplify, simplify2) {
  // create division
  std::shared_ptr<DivideOperator> div = std::make_shared<DivideOperator>(2, 1);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create final sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("3"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+(4/2) should return 3
TEST(Simplify, simplify3) {
  // create division
  std::shared_ptr<DivideOperator> div = std::make_shared<DivideOperator>(4, 2);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create final sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("3"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+(4/3) should return (7/3)
TEST(Simplify, simplify4) {
  // create division
  std::shared_ptr<DivideOperator> div = std::make_shared<DivideOperator>(4, 3);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create final sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("(7/3)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+(4/3)+(15/15) should return (10/3)
TEST(Simplify, simplify5) {
  // create division
  std::shared_ptr<DivideOperator> div = std::make_shared<DivideOperator>(4, 3);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create a new sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);
  // create a new div
  std::shared_ptr<DivideOperator> div2 =
    std::make_shared<DivideOperator>(15, 15);
  // create final sum
  std::shared_ptr<AddOperator> sumF = std::make_shared<AddOperator>(sumV, div2);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumF->simplify()->print(), Eq("(10/3)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+((12/3)/3)+(15/15) should return (10/3)
TEST(Simplify, simplify6) {
  // create division 12/3
  std::shared_ptr<DivideOperator> divi =
    std::make_shared<DivideOperator>(12, 3);
  // create integer 3
  std::shared_ptr<IntegerVariable> fi = std::make_shared<IntegerVariable>(3);
  // create division
  std::shared_ptr<DivideOperator> div =
    std::make_shared<DivideOperator>(divi, fi);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create a new sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);
  // create a new div
  std::shared_ptr<DivideOperator> div2 =
    std::make_shared<DivideOperator>(15, 15);
  // create final sum
  std::shared_ptr<AddOperator> sumF = std::make_shared<AddOperator>(sumV, div2);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumF->simplify()->print(), Eq("(10/3)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+((12/3)/3)+(73/15) should return (36/5)
TEST(Simplify, simplify7) {
  // create division 12/3
  std::shared_ptr<DivideOperator> divi =
    std::make_shared<DivideOperator>(12, 3);
  // create integer 3
  std::shared_ptr<IntegerVariable> fi = std::make_shared<IntegerVariable>(3);
  // create division
  std::shared_ptr<DivideOperator> div =
    std::make_shared<DivideOperator>(divi, fi);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create a new sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);
  // create a new div
  std::shared_ptr<DivideOperator> div2 =
    std::make_shared<DivideOperator>(73, 15);
  // create final sum
  std::shared_ptr<AddOperator> sumF = std::make_shared<AddOperator>(sumV, div2);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumF->simplify()->print(), Eq("(36/5)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (2)*(4/3) should return (8/3)
TEST(Simplify, simplify8) {
  // create division
  std::shared_ptr<DivideOperator> div = std::make_shared<DivideOperator>(4, 3);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(2);
  // create final sum
  std::shared_ptr<MultiplyOperator> sumV =
    std::make_shared<MultiplyOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("(8/3)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// 5-(((45/(78-(2*78)))+((45/2)*(23/25)))/(12*3)) should return (866/195)
TEST(Simplify, simplify9) {
  // create the small terms
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(5);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(78);
  std::shared_ptr<IntegerVariable> int3 = std::make_shared<IntegerVariable>(45);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(12, 3);
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(2, 78);
  std::shared_ptr<DivideOperator> div1 =
    std::make_shared<DivideOperator>(23, 25);
  std::shared_ptr<DivideOperator> div2 =
    std::make_shared<DivideOperator>(45, 2);

  // (45/(78-(2*78)))
  std::shared_ptr<SubstractOperator> sub1 =
    std::make_shared<SubstractOperator>(int2, mul2);
  std::shared_ptr<DivideOperator> div3 =
    std::make_shared<DivideOperator>(int3, sub1);

  // ((45/2)*(23/25))
  std::shared_ptr<MultiplyOperator> mul3 =
    std::make_shared<MultiplyOperator>(div2, div1);

  // ((45/(78-(2*78)))+((45/2)*(23/25))) (add two previous)
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(div3, mul3);

  // ((45/(78-(2*78)))+((45/2)*(23/25)))/(12*3)
  std::shared_ptr<DivideOperator> div4 =
    std::make_shared<DivideOperator>(add1, mul1);

  // 5-(((45/(78-(2*78)))+((45/2)*(23/25)))/(12*3))
  std::shared_ptr<SubstractOperator> sub2 =
    std::make_shared<SubstractOperator>(int1, div4);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sub2->simplify()->print(), Eq("(866/195)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

}  // namespace