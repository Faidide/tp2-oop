#include <gmock/gmock.h>

#include <iostream>

#include "Operators/AddOperator.h"
#include "Operators/DivideOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Operators/SubstractOperator.h"
#include "Variables/IntegerVariable.h"
#include "Variables/SymbolicVariable.h"

namespace {

using testing::Eq;

// (1)+(1/1) should return 2
TEST(SimplifySymbolic, simplify1) {
  // create division
  std::shared_ptr<DivideOperator> div = std::make_shared<DivideOperator>(1, 1);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create final sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("2"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+(2/a) should return ((a+2)/a)
TEST(SimplifySymbolic, simplify2) {
  // create division
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  std::shared_ptr<DivideOperator> div =
    std::make_shared<DivideOperator>(int1, a);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create final sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("((a+2)/a)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (a)+(4/2) should return (a+2)
TEST(SimplifySymbolic, simplify3) {
  // create division
  std::shared_ptr<DivideOperator> div = std::make_shared<DivideOperator>(4, 2);
  // create integer
  std::shared_ptr<SymbolicVariable> itg =
    std::make_shared<SymbolicVariable>("a");
  // create final sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("(a+2)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+(a/3) should return ((3+a)/3)
TEST(SimplifySymbolic, simplify4) {
  // create division
  std::shared_ptr<IntegerVariable> itgi = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  std::shared_ptr<DivideOperator> div =
    std::make_shared<DivideOperator>(a, itgi);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create final sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("((3+a)/3)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1)+(((12*a)/3)/3)+(73/15) should return (((180*a)+792)/135)
TEST(SimplifySymbolic, simplify5) {
  // create division (12*a)/3
  std::shared_ptr<IntegerVariable> iint2 =
    std::make_shared<IntegerVariable>(12);
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  std::shared_ptr<MultiplyOperator> mulDiv =
    std::make_shared<MultiplyOperator>(a, iint2);
  std::shared_ptr<IntegerVariable> iint1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<DivideOperator> divi =
    std::make_shared<DivideOperator>(mulDiv, iint1);
  // create integer 3
  std::shared_ptr<IntegerVariable> fi = std::make_shared<IntegerVariable>(3);
  // create division
  std::shared_ptr<DivideOperator> div =
    std::make_shared<DivideOperator>(divi, fi);
  // create integer
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  // create a new sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, div);
  // create a new div
  std::shared_ptr<DivideOperator> div2 =
    std::make_shared<DivideOperator>(73, 15);
  // create final sum
  std::shared_ptr<AddOperator> sumF = std::make_shared<AddOperator>(sumV, div2);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumF->simplify()->print(), Eq("((792+(a*180))/135)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1+a)+5 should return (6+a)
TEST(SimplifySymbolic, simplify6) {
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  std::shared_ptr<IntegerVariable> itg2 = std::make_shared<IntegerVariable>(5);
  // create a new sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, a);
  // create final sum
  std::shared_ptr<AddOperator> sumF = std::make_shared<AddOperator>(sumV, itg2);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumF->simplify()->print(), Eq("(6+a)"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (1+a)*5 should return (5+(5*a))
TEST(SimplifySymbolic, simplify7) {
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  std::shared_ptr<IntegerVariable> itg2 = std::make_shared<IntegerVariable>(5);
  // create a new sum
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, a);
  // create final product
  std::shared_ptr<MultiplyOperator> sumF =
    std::make_shared<MultiplyOperator>(sumV, itg2);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumF->simplify()->print(), Eq("(5+(5*a))"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (a+0) should return a
TEST(SimplifySymbolic, simplify8) {
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(0);
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  std::shared_ptr<AddOperator> sumV = std::make_shared<AddOperator>(itg, a);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("a"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (a/1) should return a
TEST(SimplifySymbolic, simplify9) {
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  std::shared_ptr<DivideOperator> sumV =
    std::make_shared<DivideOperator>(a, itg);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("a"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// (a*1) should return a
TEST(SimplifySymbolic, simplify10) {
  std::shared_ptr<IntegerVariable> itg = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  std::shared_ptr<MultiplyOperator> sumV =
    std::make_shared<MultiplyOperator>(a, itg);

  // try to display simplified add operator result
  try {
    ASSERT_THAT(sumV->simplify()->print(), Eq("a"));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

}  // namespace