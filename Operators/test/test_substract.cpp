#include <gmock/gmock.h>

#include <iostream>

#include "Operators/SubstractOperator.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(SubstractOperator, creation) {
  // test if sucessfully create a function
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  SubstractOperator mySub(int1, int2);
}

// testing the object creation
TEST(SubstractOperator, creation2) {
  // test if sucessfully create a function
  SubstractOperator mySub(1, 3);
}

// testing the object creation
TEST(SubstractOperator, simplification) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  SubstractOperator mySub(int1, int2);
  try {
    ASSERT_THAT(mySub.simplify()->print() == IntegerVariable(0).print(),
                Eq(true));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// testing the object creation
TEST(SubstractOperator, print) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  SubstractOperator monAdd(int1, int2);
  ASSERT_THAT(monAdd.print(), Eq("(1-1)"));
}

}  // namespace