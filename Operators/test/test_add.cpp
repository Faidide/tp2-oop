#include <gmock/gmock.h>

#include <iostream>

#include "Operators/AddOperator.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(AddOperator, creation) {
  // test if sucessfully create a function
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  AddOperator monAdd(int1, int2);
}

// testing the object creation
TEST(AddOperator, creation2) {
  // test if sucessfully create a function
  AddOperator monAdd(1, 3);
}

// testing the object creation
TEST(AddOperator, simplification) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  AddOperator monAdd(int1, int2);
  try {
    ASSERT_THAT(monAdd.simplify()->print() == IntegerVariable(2).print(),
                Eq(true));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

// testing the object creation
TEST(AddOperator, print) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  AddOperator monAdd(int1, int2);
  ASSERT_THAT(monAdd.print(), Eq("(1+1)"));
}

}  // namespace