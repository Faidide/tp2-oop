#include <gmock/gmock.h>

#include <iostream>

#include "Operators/AddOperator.h"
#include "Operators/DivideOperator.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(DivideOperator, creation) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  DivideOperator monDiv(int1, int2);
}

// testing the object creation
TEST(DivideOperator, creation2) { AddOperator monDiv(1, 3); }

TEST(DivideOperator, simplify) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  DivideOperator monDiv(int1, int2);
  try {
    ASSERT_THAT(monDiv.simplify()->print() == IntegerVariable(1).print(),
                Eq(true));
  } catch (const std::exception& err) {
    FAIL() << "Error at test:" << err.what() << std::endl;
  }
}

TEST(DivideOperator, print) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(1);
  DivideOperator mDiv(int1, int2);
  ASSERT_THAT(mDiv.print(), Eq("(1/1)"));
}

}  // namespace