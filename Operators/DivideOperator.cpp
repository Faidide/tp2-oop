#include "DivideOperator.h"

#include "Operators/AddOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Operators/SubstractOperator.h"

// Constructors/Destructors
//

DivideOperator::DivideOperator(
  std::shared_ptr<ArythmeticAbstract> leftOperandI,
  std::shared_ptr<ArythmeticAbstract> rightOperandI)
  : OperatorAbstract(leftOperandI, rightOperandI) {
  stringRep = "/";
}

DivideOperator::~DivideOperator() {}

//
// Methods
//

/**
 * Simplify the operator left and right operand and try to add the two if
 * possible.
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> DivideOperator::simplify() const {
  // get the simplification for left and right side of operand
  std::shared_ptr<ArythmeticAbstract> lSimplified = LeftOperand->simplify();
  std::shared_ptr<ArythmeticAbstract> rSimplified = RightOperand->simplify();
  // try to cast the simplified values to integer expressions
  IntegerVariable* lInt = dynamic_cast<IntegerVariable*>(lSimplified.get());
  IntegerVariable* rInt = dynamic_cast<IntegerVariable*>(rSimplified.get());
  // if the bottom one is a one, return the top
  if (rInt != nullptr && rInt->getValue() == 1) {
    return lSimplified;
  };
  // if they are both integers
  if (lInt != nullptr && rInt != nullptr) {
    // if one nominator modulo denominator is zero
    if (lInt->getValue() % rInt->getValue() == 0) {
      // return a new integer value which is nominator/denominator
      return std::make_shared<IntegerVariable>(lInt->getValue() /
                                               rInt->getValue());
    }
    // if they have common factor greater than 1, divide both sides by it
    int commonDivisor =
      IntegerVariable::gcd(lInt->getValue(), rInt->getValue());
    if (commonDivisor > 1) {
      return std::make_shared<DivideOperator>(lInt->getValue() / commonDivisor,
                                              rInt->getValue() / commonDivisor);
    }
  }
  // if we reach here it was not possible to make in an int

  // try to cast em to divide operators
  DivideOperator* lDiv = dynamic_cast<DivideOperator*>(lSimplified.get());
  DivideOperator* rDiv = dynamic_cast<DivideOperator*>(rSimplified.get());

  // if they are both divide operators
  if (lDiv != nullptr && rDiv != nullptr) {
    // simplify based on (a/b)/(c/d)=((a*d)/(b*c))
    return std::make_shared<DivideOperator>(
             std::make_shared<MultiplyOperator>(lDiv->LeftOperand,
                                                rDiv->RightOperand),
             std::make_shared<MultiplyOperator>(lDiv->RightOperand,
                                                rDiv->LeftOperand))
      ->simplify();
  }

  // if left(top) is integer and right(bottom) is div
  if (lInt != nullptr && rDiv != nullptr) {
    // simplify based on (a/(b/c))=((a*c)/b)
    return std::make_shared<DivideOperator>(std::make_shared<MultiplyOperator>(
                                              lSimplified, rDiv->RightOperand),
                                            rDiv->LeftOperand)
      ->simplify();
  }

  // if left(top) is div and right(bottom) is integer
  if (lDiv != nullptr && rInt != nullptr) {
    // simplify based on (a/b)/c = a/(b*c)
    return std::make_shared<DivideOperator>(lDiv->LeftOperand,
                                            std::make_shared<MultiplyOperator>(
                                              lDiv->RightOperand, rSimplified))
      ->simplify();
  }

  // if these are non reductible integer and both ends are negative, make em
  // positive
  if (lInt != nullptr && rInt != nullptr && lInt->getValue() < 0 &&
      rInt->getValue() < 0) {
    return std::make_shared<DivideOperator>(-lInt->getValue(),
                                            -rInt->getValue());
  }

  // then, return simplified denominator over denom
  return std::make_shared<DivideOperator>(lSimplified, rSimplified);
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> DivideOperator::derivative(
  std::string target) const {
  // return derivative of left side plus the rigt side one
  std::shared_ptr<ArythmeticAbstract> leftDeriv =
    LeftOperand->derivative(target)->simplify();
  std::shared_ptr<ArythmeticAbstract> rightDeriv =
    RightOperand->derivative(target)->simplify();

  // (U'V-UV')
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(leftDeriv, RightOperand);
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(rightDeriv, LeftOperand);
  std::shared_ptr<SubstractOperator> sub1 =
    std::make_shared<SubstractOperator>(mul1, mul2);

  // (V*V)
  std::shared_ptr<MultiplyOperator> lowerPart =
    std::make_shared<MultiplyOperator>(RightOperand->simplify(),
                                       RightOperand->simplify());

  // build and return the result
  return std::make_shared<DivideOperator>(sub1->simplify(),
                                          lowerPart->simplify())
    ->simplify();
}

// Accessor methods
//

// Other methods
//
/**
 * Add operators
 */
std::shared_ptr<ArythmeticAbstract> DivideOperator::operator+(
  IntegerVariable& b) {
  // copy b as a new variable we can easily handle memory and pointers with
  std::shared_ptr<IntegerVariable> bPtr =
    std::make_shared<IntegerVariable>(b.getValue());
  // create a new mult op with b*denom
  std::shared_ptr<MultiplyOperator> term1 =
    std::make_shared<MultiplyOperator>(bPtr, RightOperand);
  // create a new add op with the new mult plus the numerator
  std::shared_ptr<AddOperator> term2 =
    std::make_shared<AddOperator>(term1, LeftOperand);
  // create a new division operator with the new add as numerator and old denom
  // as denom
  std::shared_ptr<DivideOperator> term3 =
    std::make_shared<DivideOperator>(term2, RightOperand);

  // return it
  return term3;
}

std::shared_ptr<ArythmeticAbstract> DivideOperator::operator+(
  ArythmeticAbstract& b) {
  // TODO:
  // simplify value b
  // if value b can get casted to an integer
  // use the implemented integer addition
  // if value b can get casted to a divide operator
  // call the yet to implement divide operator add

  // bad practice (TODO: find alternative)
  std::shared_ptr<ArythmeticAbstract> myPtr(&b);

  // return a new addoperator with both elements
  return std::make_shared<AddOperator>(
    std::make_shared<DivideOperator>(LeftOperand, RightOperand), myPtr);
}

std::shared_ptr<ArythmeticAbstract> DivideOperator::operator+(
  DivideOperator& b) {
  // we are going to do (a/b)+(c/d) = ((a*d)+(c*b))/(b*d)

  // let's first create two multiplyOperators for the top sums
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(LeftOperand, b.RightOperand);
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(b.LeftOperand, RightOperand);
  // and create a AddOperator for them
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(mul1, mul2);
  // and one MultiplyOperator for the bottom multiplication
  std::shared_ptr<MultiplyOperator> mul3 =
    std::make_shared<MultiplyOperator>(RightOperand, b.RightOperand);

  // finally we will return a divideOperator
  return std::make_shared<DivideOperator>(add1, mul3);
}

std::shared_ptr<ArythmeticAbstract> DivideOperator::operator*(
  IntegerVariable& b) {
  // make a copy of the integer to put it in the result
  std::shared_ptr<IntegerVariable> int1 =
    std::make_shared<IntegerVariable>(b.getValue());
  // return the result (we multiply this's numerator with the b and return
  // another divideOp)
  return std::make_shared<DivideOperator>(
    std::make_shared<MultiplyOperator>(int1, this->LeftOperand),
    this->RightOperand);
}

std::shared_ptr<ArythmeticAbstract> DivideOperator::operator*(
  DivideOperator& b) {
  return std::make_shared<DivideOperator>(
    std::make_shared<MultiplyOperator>(b.LeftOperand, LeftOperand),
    std::make_shared<MultiplyOperator>(b.RightOperand, RightOperand));
}

std::shared_ptr<ArythmeticAbstract> DivideOperator::operator*(
  ArythmeticAbstract& b) {
  // will not be necessary to implement
  return nullptr;
}

std::shared_ptr<ArythmeticAbstract> DivideOperator::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else, return a new operator with both side substituted
  return std::make_shared<DivideOperator>(LeftOperand->substitute(x, y),
                                          RightOperand->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float DivideOperator::evaluate() const {
  if (RightOperand->evaluate() == 0.0)
    throw std::runtime_error("Null division error!");
  return (LeftOperand->evaluate() / RightOperand->evaluate());
}