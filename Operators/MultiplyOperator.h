
#ifndef MULTIPLYOPERATOR_H
#define MULTIPLYOPERATOR_H

#include <string>

#include "Operators/OperatorAbstract.h"

/**
 * class MultiplyOperator
 *
 */

class MultiplyOperator : virtual public OperatorAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  MultiplyOperator(std::shared_ptr<ArythmeticAbstract> leftOperandI,
                   std::shared_ptr<ArythmeticAbstract> rightOperandI);

  MultiplyOperator(int a, int b) : OperatorAbstract(a, b) { stringRep = "*"; };

  /**
   * Empty Destructor
   */
  virtual ~MultiplyOperator();

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  /**
   * Simplify the operator left and right operand and try to add the two if
   * possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const override final;

  /**
   * Compute the derivate given a variable name.
   * @return ArythmeticAbstract
   * @param  target Variable to derivate the expression with.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(
    std::string target) const override final;

  // return an integer multiple if can find one
  ArythmeticAbstract* findIntegerMultiple();

  /**
   * Replace some expression x with another y inside the ArythmeticAbstract
   *
   */
  std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract> x,
    std::shared_ptr<ArythmeticAbstract> y);

  /**
   * Evaluate the float value of the expression
   * @return the floating point value of the expression
   */
  float evaluate() const;

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //
};

#endif  // MULTIPLYOPERATOR_H
