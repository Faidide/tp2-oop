#include "Operators/AddOperator.h"

#include "Operators/DivideOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Variables/SymbolicVariable.h"

// Constructors/Destructors
//

AddOperator::AddOperator(std::shared_ptr<ArythmeticAbstract> leftOperandI,
                         std::shared_ptr<ArythmeticAbstract> rightOperandI) {
  LeftOperand = leftOperandI;
  RightOperand = rightOperandI;
  stringRep = "+";
}

AddOperator::~AddOperator() {}

//
// Methods
//
/**
 * Simplify the operator left and right operand and try to add the two if
 * possible.
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> AddOperator::simplify() const {
  // get the simplification for left and right side of operand
  std::shared_ptr<ArythmeticAbstract> lSimplified = LeftOperand->simplify();
  std::shared_ptr<ArythmeticAbstract> rSimplified = RightOperand->simplify();

  // try to cast the simplified values to integer expressions
  IntegerVariable* lInt = dynamic_cast<IntegerVariable*>(lSimplified.get());
  IntegerVariable* rInt = dynamic_cast<IntegerVariable*>(rSimplified.get());

  // if it suceeded, then return the added result
  if (lInt != nullptr && rInt != nullptr) {
    return std::make_shared<IntegerVariable>(lInt->getValue() +
                                             rInt->getValue());
  }

  // if one of the two is zero, return the other
  if (lInt != nullptr && lInt->getValue() == 0) {
    return rSimplified;
  }
  if (rInt != nullptr && rInt->getValue() == 0) {
    return lSimplified;
  }

  IntegerVariable* intVar = nullptr;
  ArythmeticAbstract* unknownVar = nullptr;
  // if right is integer and left null
  if (rInt != nullptr && lInt == nullptr) {
    // assign em
    intVar = rInt;
    unknownVar = lSimplified.get();
    // if they are in different order
  } else if (rInt == nullptr && lInt != nullptr) {
    // assign em in reverse order
    intVar = lInt;
    unknownVar = rSimplified.get();
  }

  // if left is integer and right null
  if (intVar != nullptr) {
    // try to cast right to divide operator
    DivideOperator* divideCast = dynamic_cast<DivideOperator*>(unknownVar);
    // if it suceeded (we will add em together)
    if (divideCast != nullptr) {
      // return the added terms
      return ((*divideCast) + (*intVar)).get()->simplify();
    }
  }

  // try to cast the simplified to two division
  // try to cast the simplified values to integer expressions
  DivideOperator* lFrac = dynamic_cast<DivideOperator*>(lSimplified.get());
  DivideOperator* rFrac = dynamic_cast<DivideOperator*>(rSimplified.get());

  // if they are both divisions
  if (lFrac != nullptr && rFrac != nullptr) {
    // return the simplified result of the sum
    return ((*lFrac) + (*rFrac))->simplify();
  }

  // try to cast to symbolic variables
  SymbolicVariable* lSym = dynamic_cast<SymbolicVariable*>(lSimplified.get());
  SymbolicVariable* rSym = dynamic_cast<SymbolicVariable*>(rSimplified.get());

  // if they are both symbolic and have similar stringrep
  if (lSym != nullptr && rSym != nullptr &&
      lSym->getStringRep() == rSym->getStringRep()) {
    return std::make_shared<MultiplyOperator>(
      std::make_shared<SymbolicVariable>(*lSym),
      std::make_shared<IntegerVariable>(2));
  }

  // if left one is integer
  if (lInt != nullptr) {
    // get a pointer to an integer in right exp is exists
    IntegerVariable* sumP =
      dynamic_cast<IntegerVariable*>(rSimplified->findSummedInteger());
    // if it's not null
    if (sumP != nullptr) {
      // add left int val to the value at pointer
      sumP->setValue(sumP->getValue() + lInt->getValue());
      // return the simplified version of rSimplified
      return rSimplified->simplify();
    }
  }

  // if right one is integer
  if (rInt != nullptr) {
    // get a pointer to an integer inside the left exp if exists
    IntegerVariable* sumP =
      dynamic_cast<IntegerVariable*>(lSimplified->findSummedInteger());
    // if it's not null
    if (sumP != nullptr) {
      // add right int val to the value at pointer
      sumP->setValue(sumP->getValue() + rInt->getValue());
      // return content of left expression simplified
      return lSimplified->simplify();
    }
  }

  // we will cover the simple case simplification like (x*6)+(x*7)=(x*13)
  // to cover all cases we would need to do a more complex research and we have
  // no time for that
  //
  // try to cast to multiplyOperators
  MultiplyOperator* lMul = dynamic_cast<MultiplyOperator*>(lSimplified.get());
  MultiplyOperator* rMul = dynamic_cast<MultiplyOperator*>(rSimplified.get());
  // if they are both multiplications
  if (lMul != nullptr && rMul != nullptr) {
    // if A-left equal B-left
    if (lMul->getLeftOperand()->print() == rMul->getLeftOperand()->print()) {
      return std::make_shared<MultiplyOperator>(
               lMul->getLeftOperand(),
               std::make_shared<AddOperator>(lMul->getRightOperand(),
                                             rMul->getRightOperand()))
        ->simplify();
    }
    // if A-left equal B-right
    if (lMul->getLeftOperand()->print() == rMul->getRightOperand()->print()) {
      return std::make_shared<MultiplyOperator>(
               lMul->getLeftOperand(),
               std::make_shared<AddOperator>(lMul->getRightOperand(),
                                             rMul->getLeftOperand()))
        ->simplify();
    }
    // if A-right equal B-right
    if (lMul->getRightOperand()->print() == rMul->getRightOperand()->print()) {
      return std::make_shared<MultiplyOperator>(
               lMul->getRightOperand(),
               std::make_shared<AddOperator>(lMul->getLeftOperand(),
                                             rMul->getLeftOperand()))
        ->simplify();
    }
    // if A-right equal B-left
    if (lMul->getRightOperand()->print() == rMul->getLeftOperand()->print()) {
      return std::make_shared<MultiplyOperator>(
               lMul->getRightOperand(),
               std::make_shared<AddOperator>(lMul->getLeftOperand(),
                                             rMul->getRightOperand()))
        ->simplify();
    }
  }

  // if nothing works, just return a new add operator with lSimplified and
  // rSimplified
  return std::make_shared<AddOperator>(lSimplified, rSimplified);
}

ArythmeticAbstract* AddOperator::findSummedInteger() {
  // try to cast the left and right values to integer expressions
  IntegerVariable* lInt = dynamic_cast<IntegerVariable*>(LeftOperand.get());
  IntegerVariable* rInt = dynamic_cast<IntegerVariable*>(RightOperand.get());
  // if one side is an integer, return a pointer
  if (lInt != nullptr) {
    return lInt;
  }
  if (rInt != nullptr) {
    return rInt;
  }

  // try to cast em to sums
  AddOperator* lSum = dynamic_cast<AddOperator*>(LeftOperand.get());
  AddOperator* rSum = dynamic_cast<AddOperator*>(RightOperand.get());
  // if right is a sum, call the findSummedInteger on it
  ArythmeticAbstract* res;
  if (rSum != nullptr) res = rSum->findSummedInteger();
  // return the result if it's not nullptr
  if (res != nullptr) return res;
  // do the same with left one
  if (lSum != nullptr) res = lSum->findSummedInteger();
  // return the result if it's not nullptr
  if (res != nullptr) return res;

  // if nothing matched, return nullptr
  return nullptr;
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> AddOperator::derivative(
  std::string target) const {
  // return derivative of left side plus the rigt side one
  std::shared_ptr<ArythmeticAbstract> leftDeriv =
    LeftOperand->derivative(target)->simplify();
  std::shared_ptr<ArythmeticAbstract> rightDeriv =
    RightOperand->derivative(target)->simplify();
  // return the sum
  return std::make_shared<AddOperator>(leftDeriv, rightDeriv)->simplify();
}

std::shared_ptr<ArythmeticAbstract> AddOperator::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else, return a new operator with both side substituted
  return std::make_shared<AddOperator>(LeftOperand->substitute(x, y),
                                       RightOperand->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float AddOperator::evaluate() const {
  return (LeftOperand->evaluate() + RightOperand->evaluate());
}