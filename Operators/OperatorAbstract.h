
#ifndef OPERATORABSTRACT_H
#define OPERATORABSTRACT_H

#include <iostream>
#include <memory>
#include <string>

#include "BaseArythmetic/ArythmeticAbstract.h"
#include "Variables/IntegerVariable.h"

/**
 * class OperatorAbstract
 *
 */

class OperatorAbstract : virtual public ArythmeticAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Constructors
   */
  OperatorAbstract(std::shared_ptr<ArythmeticAbstract> leftOperandI,
                   std::shared_ptr<ArythmeticAbstract> rightOperandI)
    : LeftOperand{leftOperandI}, RightOperand(rightOperandI) {}
  OperatorAbstract() {
    RightOperand = nullptr;
    LeftOperand = nullptr;
  }

  OperatorAbstract(int a, int b) {
    RightOperand = std::make_shared<IntegerVariable>(b);
    LeftOperand = std::make_shared<IntegerVariable>(a);
  }

  /**
   * Empty Destructor
   */
  virtual ~OperatorAbstract() {}

  // Static Public attributes
  //

  // Public methods

  /**
   * return the result as simplified as possible.
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const = 0;

  /**
   * Override the function to print the operation.
   * @return std::string
   */
  std::string print() const override final {
    return "(" + LeftOperand->print() + stringRep + RightOperand->print() + ")";
  };
  //

  /**
   * Return if existing a pointer to an integer directly added to the expression
   * (will be overwritten only in the add operator)
   */
  virtual ArythmeticAbstract* findSummedInteger() { return nullptr; };
  virtual ArythmeticAbstract* findIntegerMultiple() { return nullptr; };

  // Public attribute accessor methods
  /**
   * Get the value of LeftOperand
   * Left part of the operator expression.
   * @return the value of LeftOperand
   */
  std::shared_ptr<ArythmeticAbstract> getLeftOperand() const {
    return LeftOperand;
  }

  /**
   * Get the value of RightOperand
   * Right part of the operands.
   * @return the value of RightOperand
   */
  std::shared_ptr<ArythmeticAbstract> getRightOperand() const {
    return RightOperand;
  }
  //

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Left part of the operator expression.
  std::shared_ptr<ArythmeticAbstract> LeftOperand;
  // Right part of the operands.
  std::shared_ptr<ArythmeticAbstract> RightOperand;
  // string to represent the operator(+,-,*,/)
  std::string stringRep;

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

  /**
   * Set the value of LeftOperand
   * Left part of the operator expression.
   * @param value the new value of LeftOperand
   */
  void setLeftOperand(std::shared_ptr<ArythmeticAbstract> value) {
    LeftOperand = value;
  }

  /**
   * Set the value of RightOperand
   * Right part of the operands.
   * @param value the new value of RightOperand
   */
  void setRightOperand(std::shared_ptr<ArythmeticAbstract> value) {
    RightOperand = value;
  }

  /**
   * Set the value of stringRep
   * @param value the new value of stringRep
   */
  void setStringRep(std::string value) { stringRep = value; }

  /**
   * Get the value of stringRep
   * @return the value of stringRep
   */
  std::string getStringRep() const { return stringRep; }

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //

  void initAttributes();
};

#endif  // OPERATORABSTRACT_H
