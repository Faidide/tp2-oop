#include "Variables/SymbolicVariable.h"

class Pi : public SymbolicVariable {
 public:
  Pi() : SymbolicVariable("pi", 3.14159265359) {}
  ~Pi() {}
};

class Exp : public SymbolicVariable {
 public:
  Exp() : SymbolicVariable("e", 2.71828182846) {}
  ~Exp() {}
};