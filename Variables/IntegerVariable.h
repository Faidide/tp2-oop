
#ifndef INTEGERVARIABLE_H
#define INTEGERVARIABLE_H

#include <string>

#include "Variables/VariableAbstract.h"

/**
 * class IntegerVariable
 *
 */

class IntegerVariable : public VariableAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  IntegerVariable();
  IntegerVariable(int variable);
  IntegerVariable(const IntegerVariable& target);

  /**
   * Empty Destructor
   */
  ~IntegerVariable();

  // Static Public attribute
  static int gcd(int, int);
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  // Public methods
  /**
   * Override displaying function
   * @return std::string
   */
  std::string print() const override final;

  /**
   * Override simplify function
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const override final;

  /**
   * Replace some expression x with another y inside the ArythmeticAbstract
   *
   */
  std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract> x,
    std::shared_ptr<ArythmeticAbstract> y);

  /**
   * Return if existing a pointer to an integer directly added to the expression
   * (will be overwritten only in the integer variable)
   */
  ArythmeticAbstract* findSummedInteger();
  ArythmeticAbstract* findIntegerMultiple();

  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(
    std::string target) const override final;

  /**
   * operators
   */
  std::shared_ptr<ArythmeticAbstract> operator+(IntegerVariable& b);
  std::shared_ptr<ArythmeticAbstract> operator+(ArythmeticAbstract& b);
  std::shared_ptr<ArythmeticAbstract> operator-(IntegerVariable& b);
  std::shared_ptr<ArythmeticAbstract> operator-(ArythmeticAbstract& b);
  std::shared_ptr<ArythmeticAbstract> operator*(IntegerVariable& b);
  std::shared_ptr<ArythmeticAbstract> operator*(ArythmeticAbstract& b);
  std::shared_ptr<ArythmeticAbstract> operator/(IntegerVariable& b);
  std::shared_ptr<ArythmeticAbstract> operator/(ArythmeticAbstract& b);
  /**
   * Get the value of value
   * @return the value of value
   */
  int getValue() { return value; }

  /**
   * Set the value of value
   * @param value the new value of value
   */
  void setValue(int val) { value = val; }

  /**
   * assignment operator
   */
  IntegerVariable operator=(const int b);

  // equality operator
  bool operator==(const IntegerVariable& left);

  /**
   * Evaluate the float value of the expression
   * @return the floating point value of the expression
   */
  float evaluate() const;

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  int value;

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //

  void initAttributes();
};

#endif  // INTEGERVARIABLE_H
