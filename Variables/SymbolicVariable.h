
#ifndef SYMBOLICVARIABLE_H
#define SYMBOLICVARIABLE_H

#include <string>

#include "Variables/VariableAbstract.h"

/**
 * class SymbolicVariable
 *
 */

class SymbolicVariable : public VariableAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Constructor
   */
  SymbolicVariable(std::string);
  SymbolicVariable(std::string, float);

  /**
   * Empty Destructor
   */
  ~SymbolicVariable();

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  float getValue();
  std::string getStringRep();
  bool getCanEvaluate();
  //

  // Public attribute accessor methods
  //

  /**
   * Return the variable (self).
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const override final;

  /**
   * Return the string representing the variable.
   * @return std::string
   */
  std::string print() const override final;

  /**
   * Replace some expression x with another y inside the ArythmeticAbstract
   *
   */
  std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract> x,
    std::shared_ptr<ArythmeticAbstract> y);

  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(
    std::string target) const override final;

  /**
   * Evaluate the float value of the expression
   * @return the floating point value of the expression
   */
  float evaluate() const;

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  std::string stringRep;
  float value;
  bool canEvaluate;
  //

  // Private attribute accessor methods
  void setValue(float);
  void setStringRep(std::string);
  void setCanEvaluate(bool);
  //

  // Private attribute accessor methods
  //
};

#endif  // SYMBOLICVARIABLE_H
