#include "Variables/IntegerVariable.h"

// Constructors/Destructors
//

IntegerVariable::IntegerVariable() { initAttributes(); }

IntegerVariable::IntegerVariable(int variable) : value{variable} {
  Reduced = true;
};

IntegerVariable::IntegerVariable(const IntegerVariable& target) {
  value = target.value;
  Reduced = true;
};

IntegerVariable::~IntegerVariable() {}

//
// Methods
//

/**
 * Override simplify function, an integer return itself and cannot be simplified
 */
std::shared_ptr<ArythmeticAbstract> IntegerVariable::simplify() const {
  return std::make_shared<IntegerVariable>(*this);
}

/**
 * Return if existing a pointer to an integer directly added to the expression
 * (will be overwritten only in the integer variable)
 */
ArythmeticAbstract* IntegerVariable::findSummedInteger() { return this; }

// return self for integers
ArythmeticAbstract* IntegerVariable::findIntegerMultiple() { return this; }

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> IntegerVariable::derivative(
  std::string target) const {
  return std::make_shared<IntegerVariable>(0);
}

bool IntegerVariable::operator==(const IntegerVariable& left) {
  return (left.value == value);
}

std::string IntegerVariable::print() const { return std::to_string(value); }
// Accessor methods
//

// Other methods
//

void IntegerVariable::initAttributes() {
  value = 0;
  Reduced = true;
}

IntegerVariable IntegerVariable::operator=(const int b) { value = b; }

std::shared_ptr<ArythmeticAbstract> IntegerVariable::operator+(
  IntegerVariable& b) {
  return std::make_shared<IntegerVariable>(value + b.value);
}

std::shared_ptr<ArythmeticAbstract> IntegerVariable::operator+(
  ArythmeticAbstract& b) {
  // TODO
  return nullptr;
}

std::shared_ptr<ArythmeticAbstract> IntegerVariable::operator-(
  IntegerVariable& b) {
  return std::make_shared<IntegerVariable>(value - b.value);
}

std::shared_ptr<ArythmeticAbstract> IntegerVariable::operator-(
  ArythmeticAbstract& b) {
  // TODO
  return nullptr;
}

std::shared_ptr<ArythmeticAbstract> IntegerVariable::operator*(
  IntegerVariable& b) {
  return std::make_shared<IntegerVariable>(value * b.value);
}

std::shared_ptr<ArythmeticAbstract> IntegerVariable::operator*(
  ArythmeticAbstract& b) {
  // TODO
  return nullptr;
}

std::shared_ptr<ArythmeticAbstract> IntegerVariable::operator/(
  IntegerVariable& b) {
  return std::make_shared<IntegerVariable>(value / b.value);
}

std::shared_ptr<ArythmeticAbstract> IntegerVariable::operator/(
  ArythmeticAbstract& b) {
  // TODO
  return nullptr;
}

int IntegerVariable::gcd(int a, int b) {
  if (a == 0) return b;
  return gcd(b % a, a);
}

/**
 * Replace some expression x with another y inside the ArythmeticAbstract
 *
 */
std::shared_ptr<ArythmeticAbstract> IntegerVariable::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if our representation is equal to x
  if (print() == x->print()) {
    // return replaced result
    return y;
  }
  // else, return copy of self
  return std::make_shared<IntegerVariable>(value);
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float IntegerVariable::evaluate() const { return (float)value; }