#include <gmock/gmock.h>

#include "Functions/CosFunction.h"
#include "Functions/ExpFunction.h"
#include "Functions/LnFunction.h"
#include "Functions/SinFunction.h"
#include "Functions/SqrtFunction.h"
#include "Operators/AddOperator.h"
#include "Operators/DivideOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Operators/PowOperator.h"
#include "Operators/SubstractOperator.h"
#include "Variables/IntegerVariable.h"
#include "Variables/MathematicalConstants.h"

namespace {

using testing::Eq;

// float to abstract architecture imprecisions and use a range for equality
float equalityTrhes = 0.01;

// let's say x=35.45

// evaluate (x+2)*(x^2) = 47063.50862500001
TEST(Evaluate, eval1) {
  // create x+2
  std::shared_ptr<SymbolicVariable> x =
    std::make_shared<SymbolicVariable>("x", 35.45);
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(x, int1);

  // create x^2
  std::shared_ptr<PowOperator> pow1 = std::make_shared<PowOperator>(x, int1);

  // multiply both
  std::shared_ptr<MultiplyOperator> res =
    std::make_shared<MultiplyOperator>(add1, pow1);

  // because floating point precision depends on the architecture, we use
  // approximative equality
  float evaluation = res->evaluate();
  ASSERT_TRUE((evaluation > (47063.508625 - equalityTrhes) &&
               evaluation < (47063.508625 + equalityTrhes)));
}

// evaluate (x+2)+(x^2)+457 = 1751.1525
TEST(Evaluate, eval2) {
  // create x+2
  std::shared_ptr<SymbolicVariable> x =
    std::make_shared<SymbolicVariable>("x", 35.45);
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<IntegerVariable> int2 =
    std::make_shared<IntegerVariable>(457);
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(x, int1);

  // create x^2
  std::shared_ptr<PowOperator> pow1 = std::make_shared<PowOperator>(x, int1);

  // add both
  std::shared_ptr<AddOperator> preres =
    std::make_shared<AddOperator>(add1, pow1);
  std::shared_ptr<AddOperator> res =
    std::make_shared<AddOperator>(preres, int2);

  // because floating point precision depends on the architecture, we use
  // approximative equality
  float evaluation = res->evaluate();
  ASSERT_TRUE((evaluation > (1751.1525 - equalityTrhes) &&
               evaluation < (1751.1525 + equalityTrhes)));
}

// evaluate cos((x+2)+(x^2)+457) = -0.28154549
TEST(Evaluate, eval3) {
  // create x+2
  std::shared_ptr<SymbolicVariable> x =
    std::make_shared<SymbolicVariable>("x", 35.45);
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<IntegerVariable> int2 =
    std::make_shared<IntegerVariable>(457);
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(x, int1);

  // create x^2
  std::shared_ptr<PowOperator> pow1 = std::make_shared<PowOperator>(x, int1);

  // add both
  std::shared_ptr<AddOperator> preres =
    std::make_shared<AddOperator>(add1, pow1);
  std::shared_ptr<AddOperator> respre =
    std::make_shared<AddOperator>(preres, int2);
  std::shared_ptr<CosFunction> res = std::make_shared<CosFunction>(respre);

  // because floating point precision depends on the architecture, we use
  // approximative equality
  float evaluation = res->evaluate();
  ASSERT_TRUE((evaluation > (-0.28154549 - equalityTrhes) &&
               evaluation < (-0.28154549 + equalityTrhes)));
}

}  // namespace