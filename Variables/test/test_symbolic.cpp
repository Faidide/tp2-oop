#include <gmock/gmock.h>

#include "Variables/IntegerVariable.h"
#include "Variables/SymbolicVariable.h"

namespace {

using testing::Eq;

TEST(SymbolicVariable, creation) { SymbolicVariable mavar("x"); }

TEST(SymbolicVariable, creation2) { SymbolicVariable mavar("x", 3); }

TEST(SymbolicVariable, creation3) { SymbolicVariable mavar("x", 3.0); }

TEST(SymbolicVariable, print) {
  SymbolicVariable mavar("x", 3.0);
  ASSERT_THAT(mavar.print(), Eq("x"));
}

TEST(SymbolicVariable, simplify) {
  SymbolicVariable mavar("x", 3.0);
  ASSERT_THAT(mavar.simplify()->print(), Eq("x"));
}

}  // namespace