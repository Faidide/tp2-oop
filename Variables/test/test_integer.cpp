#include <gmock/gmock.h>

#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(IntegerVariable, creation) { IntegerVariable mavar; }

// testing the object creation
TEST(IntegerVariable, creation2) { IntegerVariable mavar = 2; }

TEST(IntegerVariable, creation3) {
  IntegerVariable mavar = 2;
  IntegerVariable mavar2(mavar);
  ASSERT_THAT(mavar2.getValue(), Eq(mavar.getValue()));
}

TEST(IntegerVariable, print) {
  IntegerVariable mavar = 2;
  std::string res = mavar.print();
  ASSERT_THAT(res, "2");
}

TEST(IntegerVariable, equal) {
  IntegerVariable mavar = 2;
  IntegerVariable mavar2 = 2;
  ASSERT_THAT(mavar == mavar2, Eq(true));
}

TEST(IntegerVariable, add1) {
  IntegerVariable mavar = 2;
  IntegerVariable mavar2 = 2;
  IntegerVariable res = 4;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar + mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, add2) {
  IntegerVariable mavar = 2;
  IntegerVariable mavar2 = -2;
  IntegerVariable res = 0;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar + mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, add3) {
  IntegerVariable mavar = -2;
  IntegerVariable mavar2 = -2;
  IntegerVariable res = -4;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar + mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, sub1) {
  IntegerVariable mavar = 2;
  IntegerVariable mavar2 = 2;
  IntegerVariable res = 0;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar - mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, sub2) {
  IntegerVariable mavar = 2;
  IntegerVariable mavar2 = -2;
  IntegerVariable res = 4;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar - mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, sub3) {
  IntegerVariable mavar = -2;
  IntegerVariable mavar2 = -2;
  IntegerVariable res = 0;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar - mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, mul1) {
  IntegerVariable mavar = 2;
  IntegerVariable mavar2 = 3;
  IntegerVariable res = 6;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar * mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, mul2) {
  IntegerVariable mavar = 2;
  IntegerVariable mavar2 = -2;
  IntegerVariable res = -4;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar * mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, mul3) {
  IntegerVariable mavar = -2;
  IntegerVariable mavar2 = -2;
  IntegerVariable res = 4;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar * mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, div1) {
  IntegerVariable mavar = 6;
  IntegerVariable mavar2 = 2;
  IntegerVariable res = 3;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar / mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, div2) {
  IntegerVariable mavar = 3;
  IntegerVariable mavar2 = 2;
  IntegerVariable res = 1;
  ASSERT_THAT(*dynamic_cast<IntegerVariable*>((mavar / mavar2).get()) == res,
              Eq(true));
}

TEST(IntegerVariable, gcd) {
  ASSERT_THAT(IntegerVariable::gcd(10, 20), Eq(10));
}

TEST(IntegerVariable, gcd2) {
  ASSERT_THAT(IntegerVariable::gcd(20, 10), Eq(10));
}

TEST(IntegerVariable, gcd3) { ASSERT_THAT(IntegerVariable::gcd(45, 7), Eq(1)); }

}  // namespace