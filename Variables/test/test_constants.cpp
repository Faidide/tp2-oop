#include <gmock/gmock.h>

#include "Variables/IntegerVariable.h"
#include "Variables/MathematicalConstants.h"

namespace {

using testing::Eq;

// test constants creation
TEST(MathematicalConstants, creationPi) { Pi monpi; }

TEST(MathematicalConstants, creationExp) { Exp monExp; }

}  // namespace