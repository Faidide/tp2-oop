#include "Variables/SymbolicVariable.h"

#include "Variables/IntegerVariable.h"

// Constructors/Destructors
//

SymbolicVariable::SymbolicVariable(std::string rep)
  : stringRep{rep}, canEvaluate{false} {
  // nothing to do
}

SymbolicVariable::SymbolicVariable(std::string rep, float val)
  : canEvaluate{true}, value{val}, stringRep{rep} {
  // nothing to do
}

SymbolicVariable::~SymbolicVariable() {}

//
// Methods
/**
 * Simplify the operator left and right operand and try to add the two if
 * possible. Return a copy of self for SymbolVariables.
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> SymbolicVariable::simplify() const {
  return std::make_shared<SymbolicVariable>(stringRep, value);
}

/**
 * Derivates from the target variable.
 * @return ArythmeticAbstract
 * @param  target Variable name to derivate from.
 */
std::shared_ptr<ArythmeticAbstract> SymbolicVariable::derivative(
  std::string target) const {
  // if target is equal to the stringRep, return one
  if (target == stringRep) return std::make_shared<IntegerVariable>(1);
  // else, return 0
  else
    return std::make_shared<IntegerVariable>(0);
}

/**
 * Print the variable. (Just return stringRep here)
 * @return string
 */
std::string SymbolicVariable::print() const { return stringRep; }
//
//

// Accessor methods
float SymbolicVariable::getValue() { return value; }
std::string SymbolicVariable::getStringRep() { return stringRep; }
bool SymbolicVariable::getCanEvaluate() { return canEvaluate; }
//

// Other methods
void SymbolicVariable::setValue(float val) { value = val; }
void SymbolicVariable::setStringRep(std::string val) { stringRep = val; }
void SymbolicVariable::setCanEvaluate(bool val) { canEvaluate = val; }
//

/**
 * Replace some expression x with another y inside the ArythmeticAbstract
 *
 */
std::shared_ptr<ArythmeticAbstract> SymbolicVariable::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if the representation matches
  if (stringRep == x->print()) {
    // return the replaced result
    return y;
  }
  // if not, return a copy of self
  if (canEvaluate) {
    return std::make_shared<SymbolicVariable>(stringRep, value);
  } else {
    return std::make_shared<SymbolicVariable>(stringRep);
  }
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float SymbolicVariable::evaluate() const {
  if (canEvaluate)
    return value;
  else
    throw std::runtime_error(
      "You are trying to evaluate a symbolic varic with no assigned value.");
}