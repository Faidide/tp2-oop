
#ifndef VARIABLEABSTRACT_H
#define VARIABLEABSTRACT_H

#include <string>

#include "BaseArythmetic/ArythmeticAbstract.h"

/**
 * class VariableAbstract
 *
 */

class VariableAbstract : virtual public ArythmeticAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  VariableAbstract() {}

  /**
   * Empty Destructor
   */
  virtual ~VariableAbstract() {}

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  /**
   * Override the simplification function used to return the expression and
   * compute it as much as we can.
   * @return ArythmeticAbstract
   */
  virtual std::shared_ptr<ArythmeticAbstract> simplify() const = 0;

  /**
   * Return if existing a pointer to an integer directly added to the expression
   * (will be overwritten only in the integer variable)
   */
  virtual ArythmeticAbstract* findSummedInteger() { return nullptr; };
  virtual ArythmeticAbstract* findIntegerMultiple() { return nullptr; };

  /**
   * Display function, virtual.
   */
  virtual std::string print() const = 0;

  /**
   * Replace some expression x with another y inside the ArythmeticAbstract
   *
   */
  virtual std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract> x,
    std::shared_ptr<ArythmeticAbstract> y) = 0;

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //
};

#endif  // VARIABLEABSTRACT_H
