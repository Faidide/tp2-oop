#include "Functions/CosFunction.h"

#include "Functions/SinFunction.h"
#include "Operators/MultiplyOperator.h"
#include "Variables/IntegerVariable.h"

// Constructors/Destructors
//

CosFunction::CosFunction(std::shared_ptr<ArythmeticAbstract> exp)
  : MathFunctionAbstract(exp) {
  setFunctionName("cos");
}

CosFunction::~CosFunction() {}

//
// Methods
//
/**
 * Return the variable (self).
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> CosFunction::simplify() const {
  // simplify the expression inside
  std::shared_ptr<ArythmeticAbstract> expSimp = exprInside->simplify();

  // cos(pi)=-1
  if (expSimp->print() == "pi") return std::make_shared<IntegerVariable>(-1);
  // cos(0)=1
  if (expSimp->print() == "0") return std::make_shared<IntegerVariable>(1);
  // cos(pi/2)=0
  if (expSimp->print() == "(pi/2)" || expSimp->print() == "(pi*(1/2))")
    return std::make_shared<IntegerVariable>(0);

  // as for now, return self with simplification iside
  return std::make_shared<CosFunction>(expSimp);
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> CosFunction::derivative(
  std::string target) const {
  // derivates expression inside the cos
  std::shared_ptr<ArythmeticAbstract> simpExp = exprInside->derivative(target);
  // multiply it by -1
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(-1);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(int1, simpExp);
  // make the sin(A)
  std::shared_ptr<SinFunction> sin1 = std::make_shared<SinFunction>(exprInside);
  // return the result and simplify as possible
  return std::make_shared<MultiplyOperator>(mul1, sin1)->simplify();
}

/**
 * Replace some expression x with another y inside the ArythmeticAbstract
 *
 */
std::shared_ptr<ArythmeticAbstract> CosFunction::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else return new function and subtitude what's inside
  return std::make_shared<CosFunction>(exprInside->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float CosFunction::evaluate() const { return std::cos(exprInside->evaluate()); }