#include "Functions/SinFunction.h"

#include "Functions/CosFunction.h"
#include "Operators/MultiplyOperator.h"
#include "Variables/IntegerVariable.h"

// Constructors/Destructors
//

SinFunction::SinFunction(std::shared_ptr<ArythmeticAbstract> exp)
  : MathFunctionAbstract(exp) {
  setFunctionName("sin");
}

SinFunction::~SinFunction() {}

//
// Methods
/**
 * Return the variable (self).
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> SinFunction::simplify() const {
  // simplify the expression inside
  std::shared_ptr<ArythmeticAbstract> expSimp = exprInside->simplify();

  // sin(pi)=0
  if (expSimp->print() == "pi") return std::make_shared<IntegerVariable>(0);
  // sin(0)=0
  if (expSimp->print() == "0") return std::make_shared<IntegerVariable>(0);
  // sin(pi/2)=1
  if (expSimp->print() == "(pi/2)" || expSimp->print() == "(pi*(1/2))")
    return std::make_shared<IntegerVariable>(1);
  // sin(-pi/2)=-1
  if (expSimp->print() == "((-1*pi)/2)" ||
      expSimp->print() == "((-1*pi)*(1/2))" ||
      expSimp->print() == "(-1*(pi/2))" ||
      expSimp->print() == "(-1*(pi*(1/2)))")
    return std::make_shared<IntegerVariable>(-1);

  // as for now, return self with simplification iside
  return std::make_shared<SinFunction>(expSimp);
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> SinFunction::derivative(
  std::string target) const {
  // derivates expression inside the cos
  std::shared_ptr<ArythmeticAbstract> simpExp = exprInside->derivative(target);
  // make the sin(A)
  std::shared_ptr<CosFunction> cos1 = std::make_shared<CosFunction>(exprInside);
  // return the result and simplify as possible
  return std::make_shared<MultiplyOperator>(simpExp, cos1)->simplify();
}

/**
 * Replace some expression x with another y inside the ArythmeticAbstract
 *
 */
std::shared_ptr<ArythmeticAbstract> SinFunction::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else return new function and subtitude what's inside
  return std::make_shared<SinFunction>(exprInside->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float SinFunction::evaluate() const { return std::sin(exprInside->evaluate()); }