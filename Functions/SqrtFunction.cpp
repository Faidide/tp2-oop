#include "Functions/SqrtFunction.h"

#include "Operators/DivideOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Variables/IntegerVariable.h"

// Constructors/Destructors
//

SqrtFunction::SqrtFunction(std::shared_ptr<ArythmeticAbstract> exp)
  : MathFunctionAbstract(exp) {
  setFunctionName("sqrt");
}

SqrtFunction::~SqrtFunction() {}

//
// Methods
/**
 * Return the variable (self).
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> SqrtFunction::simplify() const {
  // as for now, return self with simplification iside
  return std::make_shared<SqrtFunction>(exprInside->simplify());
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> SqrtFunction::derivative(
  std::string target) const {
  // derivate what's insique the sqrt
  std::shared_ptr<ArythmeticAbstract> derivInside =
    exprInside->derivative(target);
  // create a new sqrt and put exprInside again
  std::shared_ptr<SqrtFunction> sqrtDenom =
    std::make_shared<SqrtFunction>(exprInside);
  // create 2*sqrt(A)
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(int1, sqrtDenom);
  // create final division
  return std::make_shared<DivideOperator>(derivInside, mul1)->simplify();
}

/**
 * Replace some expression x with another y inside the ArythmeticAbstract
 *
 */
std::shared_ptr<ArythmeticAbstract> SqrtFunction::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else return new function and subtitude what's inside
  return std::make_shared<SqrtFunction>(exprInside->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float SqrtFunction::evaluate() const {
  return std::sqrt(exprInside->evaluate());
}