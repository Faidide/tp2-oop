#include "Functions/ExpFunction.h"

#include "Operators/MultiplyOperator.h"

// Constructors/Destructors
//

ExpFunction::ExpFunction(std::shared_ptr<ArythmeticAbstract> exp)
  : MathFunctionAbstract(exp) {
  setFunctionName("exp");
}

ExpFunction::~ExpFunction() {}

//
// Methods
/**
 * Return the variable (self).
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> ExpFunction::simplify() const {
  // as for now, return self with simplification iside
  return std::make_shared<ExpFunction>(exprInside->simplify());
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> ExpFunction::derivative(
  std::string target) const {
  // derivate the exprInside
  std::shared_ptr<ArythmeticAbstract> simpExp = exprInside->derivative(target);
  // return the multiplication of a new copied exprInside times the derivative
  return std::make_shared<MultiplyOperator>(
           simpExp, std::make_shared<ExpFunction>(exprInside))
    ->simplify();
}

/**
 * Replace some expression x with another y inside the ArythmeticAbstract
 *
 */
std::shared_ptr<ArythmeticAbstract> ExpFunction::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else return new function and subtitude what's inside
  return std::make_shared<ExpFunction>(exprInside->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float ExpFunction::evaluate() const { return std::exp(exprInside->evaluate()); }