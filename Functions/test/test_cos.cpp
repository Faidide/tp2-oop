#include <gmock/gmock.h>

#include "Functions/CosFunction.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(CosFunction, creation) {
  // test if sucessfully create a function
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  CosFunction moncos(int1);
}

TEST(CosFunction, printing) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  CosFunction moncos(int1);
  ASSERT_THAT(moncos.print(), Eq("cos(1)"));
}

}  // namespace