#include <gmock/gmock.h>

#include "Functions/ExpFunction.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(ExpFunction, creation) {
  // test if sucessfully create a function
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  ExpFunction monExp(int1);
}

TEST(ExpFunction, printing) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  ExpFunction monExp(int1);
  ASSERT_THAT(monExp.print(), Eq("exp(1)"));
}

}  // namespace