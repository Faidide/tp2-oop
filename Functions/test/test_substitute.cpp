#include <gmock/gmock.h>

#include "Functions/CosFunction.h"
#include "Functions/SqrtFunction.h"
#include "Operators/AddOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Variables/IntegerVariable.h"
#include "Variables/SymbolicVariable.h"

namespace {

using testing::Eq;

// replace x by y in sqrt((x*3)+4)
TEST(SubstituteFunctions, substitute1) {
  // building sqrt((x*3)+4)
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(4);
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<SymbolicVariable> y = std::make_shared<SymbolicVariable>("y");
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(mul1, int2);
  std::shared_ptr<SqrtFunction> res = std::make_shared<SqrtFunction>(add1);

  // actual test
  ASSERT_THAT(res->substitute(x, y)->print(), Eq("sqrt(((y*3)+4))"));
}

// replace x by 12 in sqrt((x*3)+4)
TEST(SubstituteFunctions, substitute2) {
  // building sqrt((x*3)+4)
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(4);
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> y = std::make_shared<IntegerVariable>(12);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(mul1, int2);
  std::shared_ptr<SqrtFunction> res = std::make_shared<SqrtFunction>(add1);

  // actual test
  ASSERT_THAT(res->substitute(x, y)->print(), Eq("sqrt(((12*3)+4))"));
}

// replace (x*3) by cos(x) in sqrt((x*3)+4)
TEST(SubstituteFunctions, substitute3) {
  // building sqrt((x*3)+4)
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(4);
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(mul1, int2);
  std::shared_ptr<SqrtFunction> res = std::make_shared<SqrtFunction>(add1);
  // building (x*3)
  std::shared_ptr<SymbolicVariable> x2 =
    std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> int3 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(x2, int3);
  // building y=cos(x)
  std::shared_ptr<SymbolicVariable> x3 =
    std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<CosFunction> y = std::make_shared<CosFunction>(x3);
  // actual test
  ASSERT_THAT(res->substitute(mul2, y)->print(), Eq("sqrt((cos(x)+4))"));
}

}  // namespace