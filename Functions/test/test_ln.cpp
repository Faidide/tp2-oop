#include <gmock/gmock.h>

#include "Functions/LnFunction.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(LnFunction, creation) {
  // test if sucessfully create a function
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  LnFunction monln(int1);
}

TEST(LnFunction, printing) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  LnFunction monln(int1);
  ASSERT_THAT(monln.print(), Eq("ln(1)"));
}

}  // namespace