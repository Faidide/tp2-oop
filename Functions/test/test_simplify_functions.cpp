#include <gmock/gmock.h>

#include "Functions/CosFunction.h"
#include "Functions/LnFunction.h"
#include "Functions/SinFunction.h"
#include "Operators/AddOperator.h"
#include "Operators/DivideOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Variables/IntegerVariable.h"
#include "Variables/MathematicalConstants.h"
#include "Variables/SymbolicVariable.h"

namespace {

using testing::Eq;

// 1+ln((3*5)*a) -> 1+ln(15*a)
TEST(SimplifyFunctions, simplify1) {
  // 1
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(1);
  // 3*5
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(3, 5);
  // a
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  // (3*5)*a
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(mul1, a);
  // ln(3*5*a)
  std::shared_ptr<LnFunction> ln = std::make_shared<LnFunction>(mul2);
  // 1+ln(3*5*a)
  std::shared_ptr<AddOperator> finalSum =
    std::make_shared<AddOperator>(in1, ln);

  // assert result
  ASSERT_THAT(finalSum->simplify()->print(), Eq("(1+ln((15*a)))"));
}

// (ln(1)/ln((3*5)*a)) -> ln(1)/ln(15*a)
TEST(SimplifyFunctions, simplify2) {
  // 3*5
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(3, 5);
  // 1
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(1);
  // ln(1)
  std::shared_ptr<LnFunction> ln1 = std::make_shared<LnFunction>(in1);
  // a
  std::shared_ptr<SymbolicVariable> a = std::make_shared<SymbolicVariable>("a");
  // (3*5)*a
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(mul1, a);
  // ln(3*5*a)
  std::shared_ptr<LnFunction> ln = std::make_shared<LnFunction>(mul2);
  // (ln(1)/ln((3*5)*a))
  std::shared_ptr<DivideOperator> finalSum =
    std::make_shared<DivideOperator>(ln1, ln);

  // assert result
  ASSERT_THAT(finalSum->simplify()->print(), Eq("(0/ln((15*a)))"));
}

// 700+cos(sin(5))+50 -> 750+cos(sin(5))
TEST(SimplifyFunctions, simplify3) {
  // 700
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(700);
  // 50
  std::shared_ptr<IntegerVariable> in2 = std::make_shared<IntegerVariable>(50);
  // 5
  std::shared_ptr<IntegerVariable> in3 = std::make_shared<IntegerVariable>(5);
  // sin(5)
  std::shared_ptr<SinFunction> sin = std::make_shared<SinFunction>(in3);
  // cos(sin(5))
  std::shared_ptr<CosFunction> cos = std::make_shared<CosFunction>(sin);
  // 700+cos(sin(5))
  std::shared_ptr<AddOperator> add1 = std::make_shared<AddOperator>(in1, cos);
  // 700+cos(sin(5))+50
  std::shared_ptr<AddOperator> finalSum =
    std::make_shared<AddOperator>(add1, in2);

  // assert result
  ASSERT_THAT(finalSum->simplify()->print(), Eq("(750+cos(sin(5)))"));
}

// ln(1)=0
TEST(SimplifyFunctions, simplify4) {
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(1);
  std::shared_ptr<LnFunction> ln = std::make_shared<LnFunction>(in1);
  // assert result
  ASSERT_THAT(ln->simplify()->print(), Eq("0"));
}

// ln(e)=1
TEST(SimplifyFunctions, simplify5) {
  Exp e;
  std::shared_ptr<LnFunction> res = std::make_shared<LnFunction>(e.simplify());
  // assert result
  ASSERT_THAT(res->simplify()->print(), Eq("1"));
}

// sin(pi)=0
TEST(SimplifyFunctions, simplify6) {
  Pi monpi;
  std::shared_ptr<SinFunction> res =
    std::make_shared<SinFunction>(monpi.simplify());
  // assert result
  ASSERT_THAT(res->simplify()->print(), Eq("0"));
}

// sin(0)=0
TEST(SimplifyFunctions, simplify7) {
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(0);
  std::shared_ptr<SinFunction> res = std::make_shared<SinFunction>(in1);
  // assert result
  ASSERT_THAT(res->simplify()->print(), Eq("0"));
}

// cos(pi/2)=0
TEST(SimplifyFunctions, simplify8) {
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(2);
  Pi monpi;
  std::shared_ptr<DivideOperator> div1 =
    std::make_shared<DivideOperator>(monpi.simplify(), in1);
  std::shared_ptr<CosFunction> res = std::make_shared<CosFunction>(div1);
  // assert result
  ASSERT_THAT(res->simplify()->print(), Eq("0"));
}

// cos(pi)=-1
TEST(SimplifyFunctions, simplify9) {
  Pi monpi;
  std::shared_ptr<CosFunction> res =
    std::make_shared<CosFunction>(monpi.simplify());
  // assert result
  ASSERT_THAT(res->simplify()->print(), Eq("-1"));
}

// sin(-pi/2)=-1
TEST(SimplifyFunctions, simplify10) {
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(2);
  Pi monpi;
  std::shared_ptr<IntegerVariable> in2 = std::make_shared<IntegerVariable>(-1);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(in2, monpi.simplify());
  std::shared_ptr<DivideOperator> div1 =
    std::make_shared<DivideOperator>(mul1, in1);
  std::shared_ptr<SinFunction> res = std::make_shared<SinFunction>(div1);
  // assert result
  ASSERT_THAT(res->simplify()->print(), Eq("-1"));
}

// cos(0)=1
TEST(SimplifyFunctions, simplify11) {
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(0);
  std::shared_ptr<CosFunction> res = std::make_shared<CosFunction>(in1);
  // assert result
  ASSERT_THAT(res->simplify()->print(), Eq("1"));
}

// sin(pi/2)=1
TEST(SimplifyFunctions, simplify12) {
  std::shared_ptr<IntegerVariable> in1 = std::make_shared<IntegerVariable>(2);
  Pi monpi;
  std::shared_ptr<DivideOperator> div1 =
    std::make_shared<DivideOperator>(monpi.simplify(), in1);
  std::shared_ptr<SinFunction> res = std::make_shared<SinFunction>(div1);
  // assert result
  ASSERT_THAT(res->simplify()->print(), Eq("1"));
}

}  // namespace