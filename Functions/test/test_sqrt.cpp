#include <gmock/gmock.h>

#include "Functions/SqrtFunction.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(SqrtFunction, creation) {
  // test if sucessfully create a function
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  SqrtFunction monsqrt(int1);
}

TEST(SqrtFunction, printing) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  SqrtFunction monsqrt(int1);
  ASSERT_THAT(monsqrt.print(), Eq("sqrt(1)"));
}

}  // namespace