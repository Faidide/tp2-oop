#include <gmock/gmock.h>

#include "Functions/SinFunction.h"
#include "Variables/IntegerVariable.h"

namespace {

using testing::Eq;

// testing the object creation
TEST(SinFunction, creation) {
  // test if sucessfully create a function
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  SinFunction monsin(int1);
}

TEST(SinFunction, printing) {
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(1);
  SinFunction monsin(int1);
  ASSERT_THAT(monsin.print(), Eq("sin(1)"));
}

}  // namespace