#include <gmock/gmock.h>

#include "Functions/CosFunction.h"
#include "Functions/ExpFunction.h"
#include "Functions/LnFunction.h"
#include "Functions/SinFunction.h"
#include "Functions/SqrtFunction.h"
#include "Operators/AddOperator.h"
#include "Operators/DivideOperator.h"
#include "Operators/MultiplyOperator.h"
#include "Operators/PowOperator.h"
#include "Operators/SubstractOperator.h"
#include "Variables/IntegerVariable.h"
#include "Variables/MathematicalConstants.h"
#include "Variables/SymbolicVariable.h"

namespace {

using testing::Eq;

// A+B -> A'+B': (3x+3)->3
TEST(Derivates, deriv1) {
  // constant to be used
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  // 3*x
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  // final sum
  std::shared_ptr<AddOperator> expr = std::make_shared<AddOperator>(mul1, int2);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("3"));
}

// A*B -> A'B+AB': ((3x)*(2x)) -> (12*x)
TEST(Derivates, deriv2) {
  // constant to be used
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<SymbolicVariable> x2 =
    std::make_shared<SymbolicVariable>("x");
  // 3*x
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(x2, int2);
  // final sum
  std::shared_ptr<MultiplyOperator> expr =
    std::make_shared<MultiplyOperator>(mul1, mul2);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(x*12)"));
}

// A*B -> A'B+AB': ((3x)*(2x)) -> (12*x)
TEST(Derivates, deriv3) {
  // constant to be used
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<SymbolicVariable> x2 =
    std::make_shared<SymbolicVariable>("x");
  // 3*x
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  std::shared_ptr<MultiplyOperator> mul2 =
    std::make_shared<MultiplyOperator>(x2, int2);
  // final sum
  std::shared_ptr<MultiplyOperator> expr =
    std::make_shared<MultiplyOperator>(mul1, mul2);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(x*12)"));
}

// A/B -> (A'B-AB')/(B*B): ((3*x)/2) -> (3/2)
TEST(Derivates, deriv4) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  // final sum
  std::shared_ptr<DivideOperator> expr =
    std::make_shared<DivideOperator>(mul1, int2);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(3/2)"));
}

// A-B -> A'-B': (3x-x)->2
TEST(Derivates, deriv5) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<SymbolicVariable> x2 =
    std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  // final sum
  std::shared_ptr<SubstractOperator> expr =
    std::make_shared<SubstractOperator>(mul1, x2);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("2"));
}

// x+b -> 1
TEST(Derivates, deriv6) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<SymbolicVariable> b = std::make_shared<SymbolicVariable>("b");
  std::shared_ptr<AddOperator> expr = std::make_shared<AddOperator>(x, b);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("1"));
}

// 3 -> 0
TEST(Derivates, deriv7) {
  std::shared_ptr<IntegerVariable> int2 = std::make_shared<IntegerVariable>(2);

  // assert result
  ASSERT_THAT(int2->derivative("x")->print(), Eq("0"));
}

// cos(3x)-> (-3*sin(3x))
TEST(Derivates, deriv8) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  // final sum
  std::shared_ptr<CosFunction> expr = std::make_shared<CosFunction>(mul1);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(-3*sin((x*3)))"));
}

// sin(3x)-> (3*cos(3x))
TEST(Derivates, deriv9) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  // final sum
  std::shared_ptr<SinFunction> expr = std::make_shared<SinFunction>(mul1);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(3*cos((x*3)))"));
}

// sqrt(x)->(1/(2*sqrt(x)))
TEST(Derivates, deriv10) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  // final sum
  std::shared_ptr<SqrtFunction> expr = std::make_shared<SqrtFunction>(x);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(1/(2*sqrt(x)))"));
}

// x^(3) -< (3*(x^2))
TEST(Derivates, deriv11) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(3);
  std::shared_ptr<PowOperator> expr = std::make_shared<PowOperator>(x, int1);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(3*(x^2))"));
}

// exp(2*x) -> (2*exp(2*x))
TEST(Derivates, deriv12) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  // final sum
  std::shared_ptr<ExpFunction> expr = std::make_shared<ExpFunction>(mul1);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(2*exp((x*2)))"));
}

// ln(2*x) -> (2/(2*x))
TEST(Derivates, deriv13) {
  std::shared_ptr<SymbolicVariable> x = std::make_shared<SymbolicVariable>("x");
  std::shared_ptr<IntegerVariable> int1 = std::make_shared<IntegerVariable>(2);
  std::shared_ptr<MultiplyOperator> mul1 =
    std::make_shared<MultiplyOperator>(x, int1);
  // final sum
  std::shared_ptr<LnFunction> expr = std::make_shared<LnFunction>(mul1);

  // assert result
  ASSERT_THAT(expr->derivative("x")->print(), Eq("(2/(x*2))"));
}

}  // namespace
