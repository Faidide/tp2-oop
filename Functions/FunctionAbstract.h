
#ifndef FUNCTIONABSTRACT_H
#define FUNCTIONABSTRACT_H

#include <string>

#include "BaseArythmetic/ArythmeticAbstract.h"

/**
 * class FunctionAbstract
 *
 */

class FunctionAbstract : virtual public ArythmeticAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  FunctionAbstract() {}

  /**
   * Empty Destructor
   */
  virtual ~FunctionAbstract() {}

  // Static Public attributes
  //

  // Public attributes
  //
  /**
   * Return if existing a pointer to an integer directly added to the expression
   * (will be overwritten only in the add operator)
   */
  ArythmeticAbstract* findSummedInteger() { return nullptr; };
  ArythmeticAbstract* findIntegerMultiple() { return nullptr; };

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //
};

#endif  // FUNCTIONABSTRACT_H
