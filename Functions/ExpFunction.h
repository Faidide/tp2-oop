
#ifndef EXPFUNCTION_H
#define EXPFUNCTION_H

#include <string>

#include "Functions/MathFunctionAbstract.h"

/**
 * class ExpFunction
 *
 */

class ExpFunction : public MathFunctionAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  ExpFunction(std::shared_ptr<ArythmeticAbstract>);

  /**
   * Empty Destructor
   */
  virtual ~ExpFunction();

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  /**
   * Return the variable (self).
   * @return ArythmeticAbstract
   */
  std::shared_ptr<ArythmeticAbstract> simplify() const override final;

  /**
   * Compute the derivative as a new expresison given target.
   * @return ArythmeticAbstract
   * @param  target Variable to compute the derivative from.
   */
  std::shared_ptr<ArythmeticAbstract> derivative(
    std::string target) const override final;

  /**
   * Replace some expression x with another y inside the ArythmeticAbstract
   *
   */
  std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract> x,
    std::shared_ptr<ArythmeticAbstract> y);

  /**
   * Evaluate the float value of the expression
   * @return the floating point value of the expression
   */
  float evaluate() const;

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //
};

#endif  // EXPFUNCTION_H
