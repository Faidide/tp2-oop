
#ifndef ARYTHMETICFUNCTION_H
#define ARYTHMETICFUNCTION_H

#include <string>

#include "Functions/FunctionAbstract.h"

/**
 * class ArythmeticFunction
 *
 */

class ArythmeticFunction : public FunctionAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  ArythmeticFunction();

  /**
   * Empty Destructor
   */
  virtual ~ArythmeticFunction();

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //
};

#endif  // ARYTHMETICFUNCTION_H
