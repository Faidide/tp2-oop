
#ifndef MATHFUNCTIONABSTRACT_H
#define MATHFUNCTIONABSTRACT_H

#include <cmath>
#include <string>

#include "BaseArythmetic/ArythmeticAbstract.h"
#include "Functions/FunctionAbstract.h"

/**
 * class MathFunctionAbstract
 *
 */

class MathFunctionAbstract : virtual public ArythmeticAbstract,
                             public FunctionAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  MathFunctionAbstract(std::shared_ptr<ArythmeticAbstract> exp)
    : exprInside{exp} {};

  /**
   * Empty Destructor
   */
  virtual ~MathFunctionAbstract() {}

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  /**
   * Simplifies what's between the parenthesis and if possible the self.
   * @return ArythmeticAbstract
   */
  virtual std::shared_ptr<ArythmeticAbstract> simplify() const = 0;

  /**
   * Print the expression.
   * @return std::string
   */
  std::string print() const {
    return getFunctionName() + "(" + exprInside->print() + ")";
  }

  // getter to retrieve the function name
  std::string getFunctionName() const { return functionName; }

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  // Represents the expression between the two parenthesis calling the function.
  // This is resolved when simplifying. For example this expression might bo a
  // MultiplyOperator with IntegerVariable representing 4 and 5 inside if this
  // object is a sin(4*5) expression.
  std::shared_ptr<ArythmeticAbstract> exprInside;

  // Represents the written name of the function
  std::string functionName;

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

  /**
   * Set the value of exprInside
   * Represents the expression between the two parenthesis calling the function.
   * This is resolved when simplifying. For example this expression might bo a
   * MultiplyOperator with IntegerVariable representing 4 and 5 inside if this
   * object is a sin(4*5) expression.
   * @param value the new value of exprInside
   */
  void setExprInside(std::shared_ptr<ArythmeticAbstract> value) {
    exprInside = value;
  }

  /**
   * Get the value of exprInside
   * Represents the expression between the two parenthesis calling the function.
   * This is resolved when simplifying. For example this expression might bo a
   * MultiplyOperator with IntegerVariable representing 4 and 5 inside if this
   * object is a sin(4*5) expression.
   * @return the value of exprInside
   */
  std::shared_ptr<ArythmeticAbstract> getExprInside() const {
    return exprInside;
  }

  // setter for func name
  void setFunctionName(std::string a) { functionName = a; }

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //

  void initAttributes();
};

#endif  // MATHFUNCTIONABSTRACT_H
