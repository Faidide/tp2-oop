#include "Functions/LnFunction.h"

#include "Operators/DivideOperator.h"
#include "Variables/IntegerVariable.h"

// Constructors/Destructors
//

LnFunction::LnFunction(std::shared_ptr<ArythmeticAbstract> exp)
  : MathFunctionAbstract(exp) {
  setFunctionName("ln");
}

LnFunction::~LnFunction() {}

//
// Methods
/**
 * Return the variable (self).
 * @return ArythmeticAbstract
 */
std::shared_ptr<ArythmeticAbstract> LnFunction::simplify() const {
  // simplify the expression inside
  std::shared_ptr<ArythmeticAbstract> expSimp = exprInside->simplify();

  // ln(1)=0
  if (expSimp->print() == "1") return std::make_shared<IntegerVariable>(0);
  // ln(e)=1
  if (expSimp->print() == "e") return std::make_shared<IntegerVariable>(1);

  return std::make_shared<LnFunction>(expSimp);
}

/**
 * Compute the derivative as a new expresison given target.
 * @return ArythmeticAbstract
 * @param  target Variable to compute the derivative from.
 */
std::shared_ptr<ArythmeticAbstract> LnFunction::derivative(
  std::string target) const {
  // get derivative of expression inside ln
  std::shared_ptr<ArythmeticAbstract> derivExpr =
    exprInside->derivative(target);
  // return the division with the result
  return std::make_shared<DivideOperator>(derivExpr, exprInside)->simplify();
}

/**
 * Replace some expression x with another y inside the ArythmeticAbstract
 *
 */
std::shared_ptr<ArythmeticAbstract> LnFunction::substitute(
  std::shared_ptr<ArythmeticAbstract> x,
  std::shared_ptr<ArythmeticAbstract> y) {
  // if we are x
  if (print() == x->print()) {
    // return y
    return y;
  }
  // else return new function and subtitude what's inside
  return std::make_shared<LnFunction>(exprInside->substitute(x, y));
}

/**
 * Evaluate the float value of the expression
 * @return the floating point value of the expression
 */
float LnFunction::evaluate() const { return std::log(exprInside->evaluate()); }