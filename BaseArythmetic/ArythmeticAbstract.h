
#ifndef ARYTHMETICABSTRACT_H
#define ARYTHMETICABSTRACT_H

#include <memory>
#include <stdexcept>
#include <string>

/**
 * class ArythmeticAbstract
 * Abstract base class for all arythmetics expression members.
 */

class ArythmeticAbstract {
 public:
  // Constructors/Destructors
  //

  /**
   * Empty Constructor
   */
  ArythmeticAbstract() {}

  /**
   * Empty Destructor
   */
  virtual ~ArythmeticAbstract() {}

  // Static Public attributes
  //

  // Public attributes
  //

  // Public attribute accessor methods
  //

  // Public attribute accessor methods
  //

  /**
   * Allow to simplify, evaluate and return the arythmetic expression at
   * maximum.
   * @return ArythmeticAbstract
   */
  virtual std::shared_ptr<ArythmeticAbstract> simplify() const = 0;

  /**
   * Return if existing a pointer to an integer directly added to the expression
   *
   */
  virtual ArythmeticAbstract* findSummedInteger() = 0;
  virtual ArythmeticAbstract* findIntegerMultiple() = 0;

  /**
   * Return the string representation the arythmetic operation.
   * @return std::string
   */
  virtual std::string print() const = 0;

  /**
   * Replace some expression with another inside the ArythmeticAbstract
   *
   */
  virtual std::shared_ptr<ArythmeticAbstract> substitute(
    std::shared_ptr<ArythmeticAbstract>,
    std::shared_ptr<ArythmeticAbstract>) = 0;

  /**
   * Evaluate the float value of the expression
   * @return the floating point value of the expression
   */
  virtual float evaluate() const = 0;

  // operators
  virtual std::shared_ptr<ArythmeticAbstract> operator+(
    ArythmeticAbstract& b) const {
    return nullptr;
  };
  virtual std::shared_ptr<ArythmeticAbstract> operator-(
    ArythmeticAbstract& b) const {
    return nullptr;
  };
  virtual std::shared_ptr<ArythmeticAbstract> operator/(
    ArythmeticAbstract& b) const {
    return nullptr;
  };
  virtual std::shared_ptr<ArythmeticAbstract> operator*(
    ArythmeticAbstract& b) const {
    return nullptr;
  };

  /**
   * Derivates the expression using a variable name.
   * @return ArythmeticAbstract
   * @param  target the parameter to derivate from.
   */
  virtual std::shared_ptr<ArythmeticAbstract> derivative(
    std::string target = "x") const = 0;

  /**
   * Get the value of Reduced
   * @return the value of Reduced
   */
  bool getReduced() const { return Reduced; }

 protected:
  // Static Protected attributes
  //

  // Protected attributes
  //

  bool Reduced;

  // Protected attribute accessor methods
  //

  // Protected attribute accessor methods
  //

  /**
   * Set the value of Reduced
   * @param value the new value of Reduced
   */
  void setReduced(bool value) { Reduced = value; }

 private:
  // Static Private attributes
  //

  // Private attributes
  //

  // Private attribute accessor methods
  //

  // Private attribute accessor methods
  //

  void initAttributes() { Reduced = false; }
};

#endif  // ARYTHMETICABSTRACT_H
